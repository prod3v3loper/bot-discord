/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const {
    EmbedBuilder
} = require('discord.js');

const Social = require('../../models/social.js');
const Coins = require('../../models/coin.js');

const COIN = require('./coin.js');
const MSG = require('./msg.js');

module.exports = class SOCIAL {

    static async create(channel, message, words, type, userCoin) {

        let data = {};
        data[type] = words[2];

        console.log(data);

        const queryres = Social.create({
            userId: message.author.id,
            data: JSON.stringify(data)
        });

        const errormsg = new EmbedBuilder()
            .setColor('#FF0000')
            .setTitle('Ohhh!')
            .setDescription('Something went wrong with adding your social. Try again later or inform the admin!');

        if (!queryres) return MSG.post(channel, errormsg);

        const res = Coins.update({
            coin: Number(userCoin.coin) + Number(500),
            dailyAt: Date.now() + 86400000
        }, {
            where: { userId: message.author.id }
        });

        const successmsg = new EmbedBuilder()
            .setColor('#00FF00')
            .setTitle('Congratulations!')
            .setDescription('✅ You add your ' + type + ' acoount successfully');

        return MSG.post(channel, successmsg);
    }

    static async read(userId) {
        return await Social.findOne({ where: { userId: userId } });
    }

    static async update(channel, message, words, type) {

        // Define
        let userMention = message.mentions.users.first();

        // Checks
        if (!userMention) return channel.reply({ content: 'No user mention found!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));

        const user = this.read(userMention.id);
        if (!user) return channel.reply({ content: 'No user found', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        user.data[type] = words[2];

        const sUpdate = await Social.update({ data: JSON.stringify(user.data) }, { where: { userId: userMention.id } });

        if (!sUpdate) return channel.reply({ content: 'Something went wrong with updating user social!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return channel.reply({ content: ':white_check_mark: User social successfully!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static delete(channel, message, address) {

        // Define
        let userMention = message.mentions.users.first();

        // Checks
        if (!userMention) return message.reply({ content: 'No user mention was specified for `.wallet remove @user`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const queryres = Social.destroy({ where: { userId: userMention.id } });

        if (!queryres) return channel.reply({ content: 'Something went wrong with deleting ' + address + ' wallet!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return channel.reply({ content: 'Wallet ' + address + ' removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static kill() {
        Social.destroy({ where: {}, truncate: true });
    }

    static async findAll() {
        return await Social.findAll();
    }

};