/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config({ override: true });

const { token, guildId, clientId } = require('../../config.json');

const MySQL = require('../../config/connect.js');

const Wallets = require('../../models/wallet.js');

module.exports = class MYSQL {

    /**
     * 
     * @param {*} args 
     */
    static async insert(args) {

        try {
            const result = await new Promise((resolve) => {
                MySQL.query("INSERT INTO " + process.env.DB_PREFIX + process.env.DB_TABLE + " (userID, username, wallet, data) VALUES ('" + args.userId + "', '" + args.username + "', '" + args.wallet + "', '" + JSON.stringify(args.data) + "')", function (err, results, arr) {
                    if (err) return console.log(err);
                    // console.log('The insert result is: ', results);
                    // console.log(arr.length + "1 record inserted");
                    resolve(results);
                });
            });

            return result.length;
        } catch (e) {
            console.log(e);
        }

        MySQL.end();

        return false;
    }

    /**
     * 
     * @param {*} userId 
     * @returns 
     */
    static async select(userId) {

        try {
            const result = await new Promise((resolve) => {
                MySQL.query('SELECT userID FROM ' + process.env.DB_PREFIX + process.env.DB_TABLE + ' WHERE userID = ' + userId, (err, results, arr) => {
                    if (err) return console.log(err);
                    // console.log('The select result is: ', results);
                    // console.log(arr.length + " record selected");
                    resolve(results);
                });
            });

            return result.length;
        } catch (e) {
            console.log(e);
        }

        MySQL.end();
        return false;
    }

    /**
     * 
     * @param {*} args 
     */
    static async update(args) {
        try {
            const result = await new Promise((resolve) => {
                MySQL.query('UPDATE ' + process.env.DB_PREFIX + process.env.DB_TABLE + ' SET data = ' + args.data + ' WHERE userID = ' + userId, (err, results, arr) => {
                    if (err) return console.log(err);
                    // console.log('The select result is: ', results);
                    // console.log(arr.length + " record selected");
                    resolve(results);
                });
            });

            return result.length;
        } catch (e) {
            console.log(e);
        }

        MySQL.end();
        return false;
    }

    /**
     * 
     * @param {*} userId 
     * @returns 
     */
    static async delete(userId) {
        try {
            const result = await new Promise((resolve) => {
                MySQL.query('DELETE FROM ' + process.env.DB_PREFIX + process.env.DB_TABLE + ' WHERE userID = ' + userId, (err, results, arr) => {
                    if (err) return console.log(err);
                    // console.log('The select result is: ', results);
                    // console.log(arr.length + " record selected");
                    resolve(results);
                });
            });

            return result.length;
        } catch (e) {
            console.log(e);
        }

        MySQL.end();
        return false;
    }

    /**
     * Only if table not synced
     * 
     * @param {*} bot 
     */
    static async snyc(bot) {

        const rest = await Wallets.findAll();

        rest.forEach(async (item, index, arr) => {

            const user = await bot.users.fetch(arr[index].userId);
            const guild = await bot.guilds.fetch(guildId);
            if (!guild) return console.log('Guild not found');
            const member = await guild.members.cache.find(m => m.id === arr[index].userId)

            const userest = await this.select(arr[index].userId);
            if (userest == 0 || userest == false) {

                let obj = {};
                if (typeof member != 'undefined' && typeof member.nickname != 'undefined' && member.nickname != null) {
                    obj = {
                        userId: arr[index].userId,
                        username: user.username,
                        wallet: arr[index].wallet,
                        data: { displayName: member.nickname }
                    };
                } else {
                    obj = {
                        userId: arr[index].userId,
                        username: user.username,
                        wallet: arr[index].wallet,
                        data: { displayName: null }
                    };
                }

                this.insert(obj);
            }
        });
    }
}