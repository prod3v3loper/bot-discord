/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const https = require('https');

const { EmbedBuilder } = require('discord.js');

const MSG = require('./msg.js');
const WALLET = require('./wallet.js');

/**
 * VERIFY SYSTEM
 */
module.exports = class VERIFY {

    static check(channel, user, mod = false) {

        if (user && (user.verify == null || user.verify == 0)) {
            if (mod) {
                const nowallet = new EmbedBuilder()
                    .setColor('#FF0000')
                    .setTitle(`You don't verified`)
                    .setDescription('Please verify yourself first here: <#' + process.env.VERIFYCHANNELID + '>\n\n🚨 If you are a previous member, you will need to repeat this process as well ❗️');
                MSG.post(channel, nowallet);
            }
            return false;
        }
        return true;
    }

    static process(channel, message, user, bot) {

        if (WALLET.check(channel, user, true)) {

            if (user && user.verify == 1) {

                let check = 'Check for WAX NFTs: `.wallet nfts`';
                if (user.nft) {
                    check = 'And you have <@&' + process.env.CUSTOMROLEIID + '> too.';
                }
                const embed = new EmbedBuilder()
                    .setColor('#00FF00')
                    .setTitle('Congrulations!')
                    .setDescription('✅ Your wallet exists and is already <@&' + process.env.VERIFEDROLE + '> role. ' + check + '\n\nCheck for your wallets with: `.wallets` or `/help` to show all commands.');
                return MSG.post(channel, embed);
            }

            if (user.wallet.toLowerCase().includes('.wam')) {
                this.wax(channel, message, user, bot);
            } else {
                this.binance(channel, message, user, bot);
            }

        }
    }

    static binance(channel, message, user, bot) {

        let url = 'https://api.bscscan.com/api?module=account&action=balance&address=' + user.wallet + '&apikey=' + process.env.BINANCE_API_KEY;

        const req = https.get(url, async (res) => {

            let body = '';

            res.on('data', async (chunk) => {
                body += chunk;
            });

            res.on('end', async () => {

                let response = JSON.parse(body);
                if (response && response.status === '1' && response.message === 'OK') {

                    if (WALLET.updateVerify(channel, message, 1)) {

                        const newUserRole = message.guild.roles.cache.get(process.env.UNVERIFIEDROLE);
                        if (newUserRole) message.member.roles.remove(newUserRole);

                        const role = message.guild.roles.cache.get(process.env.VERIFEDROLE);
                        if (role) message.member.roles.add(role);

                        message.react('✅');

                        const rolegiven = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Congratulations 🎉')
                            .setDescription(
                                '✅ You have get the <@&' + process.env.VERIFEDROLE + '> role!\n\nNow you can also get the ' + process.env.NAME + ' role. Requirement of it is:\n- You must own one of our NFTs in your wallet.\n\nAnd type followed command: `.wallet nfts`'
                            );
                        MSG.post(channel, rolegiven);

                        const dm = await client.users.fetch(message.author.id);
                        const embed = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('You have triggert a verify request')
                            .setThumbnail(process.env.THUMB)
                            .setAuthor({
                                name: '🎉 WELCOME 🎉',
                                iconURL: process.env.THUMB,
                                url: process.env.URL
                            })
                            .setDescription(`
Howdy ` + message.member.displayName + `,
                            
you have successfully verified ✅

🚨 PLEASE READ THIS MESSAGE UNTIL THE END ❗️

1. Now get your free NFT on WAX if you want, **CHOOSE ONE HERE** \n<#` + process.env.NFTCHANNELID + `>\n
2. And write the # **SIX DIGIT NUMBER** here in this channel \n<#` + process.env.NFTCHOOSECHANNELID + `>\n
                            
🚨 You receive your NFT after this two steps ❗️
                            
The ` + process.env.NAME + ` Team
`).setFooter({
                                text: process.env.FOOTERCOPY,
                                iconURL: process.env.FOOTERTHUMB
                            })
                            .setTimestamp();
                        dm.send({ embeds: [embed] });
                    }

                } else {
                    console.log("NO response");

                    const embed = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle('Your wallet dont exists on BSC Mainnet')
                        .setDescription(
                            ':x: https://bscscan.com/address/' +
                            user.wallet +
                            '\n\nCheck your wallet for correctness:\n**' +
                            user.wallet +
                            '**\n\nIf you entered your wallet incorrectly, contact the admin.'
                        );
                    MSG.post(channel, embed);
                }
            });

        }).on('error', function (e) {
            console.log("Got an error: ", e);
        });

        req.end();

    }

    /**
     * Verify user wallet
     * 
     * @param {*} message 
     * @param {*} user 
     * @param {*} channel 
     * @param {*} client 
     */
    static wax(channel, message, user, client) {

        const options = {
            hostname: 'wax.api.atomicassets.io',
            path: '/atomicassets/v1/assets/?owner=' + user.wallet,
            method: 'GET',
        };

        const req = https.request(options, async (res) => {

            if (res.statusCode === 200) {

                let result = '';

                res.setEncoding('utf8');
                res.on('data', async (data) => {
                    result += data;
                });

                res.on('end', async () => {

                    const answer = JSON.parse(result);
                    if (answer.data.length > 0) {

                        if (WALLET.updateVerify(channel, message, 1)) {

                            const newUserRole = message.guild.roles.cache.get(process.env.UNVERIFIEDROLE);
                            if (newUserRole) message.member.roles.remove(newUserRole);

                            const role = message.guild.roles.cache.get(process.env.VERIFEDROLE);
                            if (role) message.member.roles.add(role);

                            message.react('✅');

                            const rolegiven = new EmbedBuilder()
                                .setColor('#00FF00')
                                .setTitle('Congratulations 🎉')
                                .setDescription(
                                    '✅ You have get the <@&' + process.env.VERIFEDROLE + '> role!\n\nNow you can also get the ' + process.env.NAME + ' role. Requirement of it is:\n- You must own one of our NFTs in your wallet.\n\nAnd type followed command: `.wallet nfts`'
                                );
                            MSG.post(channel, rolegiven);

                            if (process.env.NFTCHANNELID && process.env.NFTCHOOSECHANNELID) {

                                const dm = await client.users.fetch(message.author.id);
                                const embed = new EmbedBuilder()
                                    .setColor('#00FF00')
                                    .setTitle('You have triggert a verify request')
                                    .setThumbnail(process.env.THUMB)
                                    .setAuthor({
                                        name: '🎉 WELCOME 🎉',
                                        iconURL: process.env.THUMB,
                                        url: process.env.URL
                                    })
                                    .setDescription(`
Howdy ` + message.member.displayName + `,
                                    
you have successfully verified ✅

🚨 PLEASE READ THIS MESSAGE UNTIL THE END ❗️

1. Now get your free NFT, **CHOOSE ONE HERE** \n<#` + process.env.NFTCHANNELID + `>\n
2. And write the # **SIX DIGIT NUMBER** here in this channel \n<#` + process.env.NFTCHOOSECHANNELID + `>\n
                                    
🚨 You receive your NFT after this two steps ❗️
                                    
The ` + process.env.NAME + ` Team
`).setFooter({
                                        text: process.env.FOOTERCOPY,
                                        iconURL: process.env.FOOTERTHUMB
                                    })
                                    .setTimestamp();
                                dm.send({ embeds: [embed] });
                            }
                        }
                    } else {

                        // const role = message.guild.roles.cache.get(process.env.VERIFEDROLE);
                        // message.member.roles.remove(role);

                        const embed = new EmbedBuilder()
                            .setColor('#FF0000')
                            .setTitle('Your wallet dont exists on WAX atomicassets')
                            .setDescription(
                                ':x: https://wax.atomichub.io/profile/' +
                                user.wallet +
                                '\n\nCheck your wallet for correctness:\n**' +
                                user.wallet +
                                '**\n\nIf you entered your wallet incorrectly, contact the admin.'
                            );
                        MSG.post(channel, embed);
                    }
                });
            }
        });

        req.on('error', (error) => {
            console.error(error);
        });

        req.end();
    }

    /**
     * Check if NFTs exists
     * 
     * @param {*} message 
     * @param {*} user 
     * @param {*} channel 
     */
    static nft(channel, message, user) {

        if (WALLET.check(channel, user, true)) {
            if (this.check(channel, user, true)) {

                if (user && user.nft) {
                    const embed = new EmbedBuilder()
                        .setColor('#00FF00')
                        .setTitle('Congrulations!')
                        .setDescription('✅ Your have already the <@&' + process.env.CUSTOMROLEIID + '> role!\n\nCheck for your wallets with: `.wallets` or `/help` to show all commands.');
                    return MSG.post(channel, embed);
                }

                const options = {
                    hostname: 'wax.api.atomicassets.io',
                    path: '/atomicassets/v1/assets/?owner=' + user.wallet,
                    method: 'GET',
                };

                const req = https.request(options, async (res) => {

                    if (res.statusCode === 200) {

                        let result = '';

                        res.setEncoding('utf8');
                        res.on('data', async (data) => {
                            result += data;
                        });

                        res.on('end', async () => {

                            const answer = JSON.parse(result);
                            if (answer.data.length > 0) {

                                let picked = false;
                                answer.data.forEach((element) => {
                                    // Check for nfts
                                    if (
                                        element &&
                                        element.template &&
                                        element.template.template_id != null &&
                                        ['477966', '477974', '477965', '477950', '477885', '477884', '477878', '474946', '457656', '456659'].indexOf(
                                            element.template.template_id
                                        ) >= 0
                                    ) {
                                        picked = true;
                                    }
                                });

                                if (!picked) {

                                    const nonfts = new EmbedBuilder()
                                        .setColor('#FF0000')
                                        .setTitle('In our wallet dont exists NFTs')
                                        .setDescription(
                                            ':x: https://wax.atomichub.io/profile/' +
                                            user.wallet +
                                            '\n\nThis is necessary to get the ' + process.env.NAME + ' role.\n\nCheck your wallet for correctness:\n**' +
                                            user.wallet +
                                            '**\n\nIf you entered your wallet incorrectly, contact the admin.'
                                        );

                                    WALLET.updateNft(channel, message, 'No nfts');

                                    const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID);
                                    message.member.roles.remove(role);
                                    return MSG.post(channel, nonfts);
                                }

                                if (WALLET.updateNft(channel, message, 1)) {

                                    message.delete(5000);

                                    const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID);
                                    message.member.roles.add(role);
                                    message.react('✅');
                                    const embed = new EmbedBuilder()
                                        .setColor('#00FF00')
                                        .setTitle('Congratulations 🎉')
                                        .setDescription('✅ Your wallet have ' + process.env.NAME + ' NFT.\n\nYou have get the <@&' + process.env.CUSTOMROLEIID + '> role!');
                                    MSG.post(channel, embed);
                                }

                            } else {

                                // const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID);
                                // message.member.roles.remove(role);

                                const embed = new EmbedBuilder()
                                    .setColor('#FF0000')
                                    .setTitle('Your wallet dont exists on WAX atomicassets')
                                    .setDescription(
                                        ':x: https://wax.atomichub.io/profile/' +
                                        user.wallet +
                                        '\n\nCheck your wallet for correctness:\n**' +
                                        user.wallet +
                                        '**'
                                    );
                                MSG.post(channel, embed);
                            }
                        });
                    }
                });

                req.on('error', (error) => {
                    console.error(error);
                });

                req.end();
            }
        }
    }
};