/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

module.exports = {
    name: 'guildMemberAdd',
    once: false,
    async execute(bot, member) {

        if (member) {
            // Welcome channel custom message
            const welcomechannel = await bot.channels.cache.get(process.env.WELCOMECHANNELID);
            if (welcomechannel) welcomechannel.send('Howdy, **' + member.displayName + '** 🎉 ' + process.env.WELCOMEMESSAGE + '. Follow this channel <#' + process.env.VERIFYCHANNELID + '> and i help you 🚀 if you type `/help`');

            // Post in main chat channel
            // const chatchannel = await bot.channels.cache.get(process.env.MAINCHATCHANNELID);
            // if (chatchannel) chatchannel.send('We have a new OTTOMANIA on board, say welcome to' + `**${member}** `);

            const hasRole = await member.roles.cache.has(process.env.UNVERIFIEDROLE);
            if (!hasRole) {
                // Add new user unverified role
                const newUserRole = await member.guild.roles.cache.get(process.env.UNVERIFIEDROLE);
                if (newUserRole) member.roles.add(newUserRole);
            }
        }
    }
};