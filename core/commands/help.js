/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('help')
    .setDescription('Replies with help info!'),
  async execute(interaction) {

    const embed = new EmbedBuilder()
      .setThumbnail(process.env.HELPTHUMB)
      .setColor(process.env.HELPCOLOR)
      .setAuthor({
        name: process.env.HELPAUTHORNAME,
        iconURL: process.env.HELPAUTHORTHUMB,
        url: process.env.HELPAUTHORURL
      })
      .setTitle(interaction.guild.name)
      .setDescription(
        '**USE**\n\n' +
        'Displays all **your wallets**\n`.wallets`\n\n' +
        '**Add wallet** to your account\n`.wallet add here-wallet-address`\n\n' +
        '**Verify** and get **verified** role\n`.wallet verify`\n\n' +
        'Get your **' + process.env.NAME + '** role\n`.wallet nfts`\n\n' +
        'For more info: ' + process.env.LINK + '\n\n' +
        '🚨 We support **WAX Blockchain** & **BSChain** wallets as main wallet, more soon❗️'
      )
      .setFooter({
        text: process.env.FOOTERCOPY,
        iconURL: process.env.FOOTERTHUMB
      })
      .setTimestamp();
    try {
      await interaction.reply({
        embeds: [embed],
      });
    } catch (error) {
      console.log(error);
    }
  }
};
