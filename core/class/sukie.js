/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { token, guildId, clientId } = require('../../config.json');

const { EmbedBuilder } = require('discord.js');

const MSG = require('./msg.js');

/**
 * SUKIE RANK SYSTEM
 */
module.exports = class SUKIE {

    static async gang(channel, message, emoji, bot, user) {

        if (user.bot) return false;

        const g = bot.guilds.cache.get(guildId);
        const members = await g.members.fetch();
        const member = members.find((m) => m.id === user.id);
        const role = await g.roles.fetch(process.env.EXTRAROLEID);
        if (role) member.roles.add(role);

        const sukiefam = new EmbedBuilder()
            .setColor('#ff0066')
            .setThumbnail(process.env.THUMB)
            .setAuthor({
                name: process.env.EXTRAROLENAME,
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`Congratulations ${user.username} 🎉`)
            .setDescription('You have received your **Sukie gang** role, now you still pick up your **Sukie fam** role here: <#1112354376036667452> and type `!sukie fam`');

        channel.send({ embeds: [sukiefam] }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static fam(channel, message, bot) {

        const role = message.guild.roles.cache.get(process.env.EXTRAROLE2ID);
        if (role) message.member.roles.add(role);

        const helper = new EmbedBuilder()
            .setColor('#ff0066')
            .setThumbnail(process.env.THUMB)
            .setAuthor({
                name: process.env.EXTRAROLE2NAME,
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`Congratulations ${message.author.username} 🎉`)
            .setDescription('You have received your **Sukie fam** role, now you can take further steps and receive FREE WAX NFT. To do this, follow this instruction:\n\n<#' + process.env.ACCESSCHANNEL + '>\n\nIf you are looking for help other help try this: `/help`');

        MSG.post(channel, helper);
    }
}