/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

const { Sequelize } = require('sequelize');
const db = require('./db.js');

const Giveaways = db.sequelize.define('giveaways', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    userId: {
        type: Sequelize.TEXT,
    },
    gId: { type: Sequelize.TEXT, defaultValue: 'No guildId' },
    mChId: { type: Sequelize.TEXT, defaultValue: 'No mainChId' },
    chId: { type: Sequelize.TEXT, defaultValue: 'No channelId' },
    mMsgId: { type: Sequelize.TEXT, defaultValue: 'No mainMsgId' },
    msgId: { type: Sequelize.TEXT, defaultValue: 'No msgId' },
    price: { type: Sequelize.TEXT, defaultValue: 'No price' },
    winners: { type: Sequelize.INTEGER, defaultValue: 1 },
    end: { type: Sequelize.INTEGER, defaultValue: 0 },
    duration: { type: Sequelize.INTEGER, defaultValue: 0 },
    ended: { type: Sequelize.BOOLEAN, defaultValue: false },
});

module.exports = Giveaways;