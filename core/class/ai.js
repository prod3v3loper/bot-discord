/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

module.exports = class AI {

    static answers(message) {

        const answers = [
            'Hi ' + message.author.tag + ', what can i do ?'
        ];

        const questions = {
            verify: 'You need help to VERIFY ?',
            nft: 'You need help for free NFT ?',
            ake: 'You need help for free faucet $AKE',
        };

        for (let key in questions) {
            if (message.content.toLowerCase().includes(key)) {

                message.reply('Please enter more input.').then(() => {
                    message.sned(`You've entered: ${message.content}\n\n` + questions[key]);
                });
            }
        }

        return message.reply({ content: answers[0] });
    }
};
