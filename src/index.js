/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */
const { token, guildId, clientId } = require('../config.json');

const { ShardingManager } = require('discord.js');

const manager = new ShardingManager('./src/bot.js', {
  token: token,
  totalShards: "auto",
  respawn: true
});

manager.on('shardCreate', shard => console.log(`Launched shard ${shard.id}`));

manager.spawn();