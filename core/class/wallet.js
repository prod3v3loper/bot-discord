/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const {
    Client,
    GatewayIntentBits,
    Partials,
    EmbedBuilder,
    ButtonBuilder,
    ButtonStyle,
    ActionRowBuilder
} = require('discord.js');

const bot = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildPresences,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildMessageReactions,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildMessageTyping,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.DirectMessageReactions,
        GatewayIntentBits.DirectMessageTyping,
        GatewayIntentBits.MessageContent
    ],
    partials: [
        Partials.Channel,
        Partials.Message,
        Partials.Reaction
    ]
});

const https = require('https');

const Wallets = require('../../models/wallet.js');

const MSG = require('./msg.js');
const MYSQL = require('./mysql.js');

/**
 * WALLET SYSTEM - CRUD
 */
module.exports = class WALLET {

    static create(channel, message, user, address) {

        /** @todo check length */
        if (address == '' || address == null) {
            const errormsg = new EmbedBuilder()
                .setColor('#FF0000')
                .setTitle('Ohhh!')
                .setDescription('Missing wallet. Please type `.wallet add your-wallet-address` to add.');
            return MSG.post(channel, errormsg);
        }

        if (!user) {

            const options = {
                hostname: 'wax.api.atomicassets.io',
                path: '/atomicassets/v1/assets/?owner=' + address,
                method: 'GET',
            };

            const req = https.request(options, async (res) => {

                if (res.statusCode === 200) {

                    let result = '';

                    res.setEncoding('utf8');
                    res.on('data', async (data) => {
                        result += data;
                    });

                    res.on('end', async () => {

                        const answer = JSON.parse(result);
                        // Found wallet on WAX ?
                        if (answer.data.length > 0) {

                            const queryres = Wallets.create({
                                userId: message.author.id,
                                verify: 0,
                                wallet: address,
                            });

                            const errormsg = new EmbedBuilder()
                                .setColor('#FF0000')
                                .setTitle('Ohhh!')
                                .setDescription('Something went wrong with adding your wallet. Try again later or inform the admin!');

                            if (!queryres) return MSG.post(channel, errormsg);

                            if (MYSQL && await MYSQL.select(message.author.id) == 0) {

                                /**
                                 * User profile display name: 
                                 *      message.author.username
                                 * 
                                 * Server profiles server Nickname: 
                                 *      message.member.displayName
                                 */
                                MYSQL.insert({
                                    userId: message.author.id,
                                    username: message.author.username,
                                    wallet: address,
                                    data: { displayname: message.member.displayName }
                                });
                            }

                            message.react('✅');

                            const successmsg = new EmbedBuilder()
                                .setColor('#00FF00')
                                .setTitle('Congratulations!')
                                .setDescription('✅ Your wallet added successfully\n\nNow verify your wallet with `.wallet verify`');

                            return MSG.post(channel, successmsg);

                        } else {

                            const embed = new EmbedBuilder()
                                .setColor('#FF0000')
                                .setTitle('Wallet dont exists on WAX atomicassets')
                                .setDescription(
                                    'Please try again or later\n\n:x: https://wax.atomichub.io/profile/' +
                                    address +
                                    '\n\nCheck your wallet for correctness:\n**' +
                                    address +
                                    '**\n\nIf you entered your wallet incorrectly, contact the admin.'
                                );
                            MSG.post(channel, embed);
                        }
                    });
                }
            });

            req.on('error', (error) => {
                console.error(error);
            });

            req.end();

        } else {

            let check = 'To verify your wallet type: `.wallet verify`';

            if (user.verify === 1) {
                check = 'And your wallet is **verified** too.';
            }

            const errormsg = new EmbedBuilder()
                .setColor('#00FF00')
                .setTitle('Congrulations!')
                .setDescription('✅ Your wallet is already **added**. ' + check + '\n\nCheck for your wallets with: `.wallets` or `/help` to show all commands.');
            return MSG.post(channel, errormsg);
        }
    }

    static async read(userId) {
        return await Wallets.findOne({ where: { userId: userId } });
    }

    static async update(channel, message, address) {

        // Define
        let userMention = message.mentions.users.first();

        // Checks
        if (!userMention) return channel.reply({ content: 'No user mention found!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));

        const walletUpdate = await Wallets.update({ wallet: address }, { where: { userId: userMention.id } });

        if (!walletUpdate) return channel.reply({ content: 'Something went wrong with deleting ' + address + ' wallet!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return channel.reply({ content: 'Wallet ' + address + ' removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static delete(channel, message, address) {

        // Define
        let userMention = message.mentions.users.first();

        // Checks
        if (!userMention) return message.reply({ content: 'No user mention was specified for `.wallet remove @user`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const queryres = Wallets.destroy({ where: { userId: userMention.id } });

        if (!queryres) return channel.reply({ content: 'Something went wrong with deleting ' + address + ' wallet!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return channel.reply({ content: 'Wallet ' + address + ' removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static kill() {
        Wallets.destroy({ where: {}, truncate: true });
    }

    static async findAll() {
        return await Wallets.findAll();
    }

    static async display(channel, message, user) {

        if (this.check(channel, user, true)) {
            if (user && user.verify == 1) {

                const adder = new EmbedBuilder()
                    .setColor('#000000')
                    .setThumbnail(process.env.THUMB)
                    .setTitle('Your wallets')
                    .setDescription('At the moment you can only add one wallet to your account, later you can add more.');

                const button = new ButtonBuilder()
                    .setCustomId('wallet_show')
                    .setLabel('Wallet')
                    .setEmoji('🎒')
                    .setStyle(ButtonStyle.Secondary);

                const row = new ActionRowBuilder()
                    .addComponents(button);

                return channel.send({ embeds: [adder], components: [row] });
            } else {
                const nowallet = new EmbedBuilder()
                    .setColor('#FF0000')
                    .setTitle(`You don't verified`)
                    .setDescription('Please verify yourself first here: <#' + process.env.VERIFYCHANNELID + '>\n\n🚨 If you are a previous member, you will need to repeat this process as well ❗️');
                MSG.post(channel, nowallet);
            }
        }
    }

    static check(channel, user, mod = false) {

        if (!user || user == null) {
            if (mod) {
                const nowallet = new EmbedBuilder()
                    .setColor('#FF0000')
                    .setTitle(`You don't have any wallets linked`)
                    .setDescription('Missing wallet. Please type `.wallet add your.wallet` to add.\n\n🚨 If you are a previous member, you will need to repeat this process as well ❗️');
                MSG.post(channel, nowallet);
            }
            return false;
        }
        return true;
    }

    static updateVerify(channel, message, mode) {

        const queryres = Wallets.update({ verify: mode }, { where: { userId: message.author.id } });
        const errormsg = new EmbedBuilder().setColor('#FF0000').setTitle('Ohhh!').setDescription('Something went wrong with updating verify in your account. Try again later or inform the admin.');
        MSG.resetBot(channel);
        if (!queryres) {
            MSG.post(channel, errormsg);
            return false;
        }
        return true;
    }

    static updateNft(channel, message, mode) {

        const queryres = Wallets.update({ nft: mode }, { where: { userId: message.author.id } });
        const errormsg = new EmbedBuilder().setColor('#FF0000').setTitle('Ohhh!').setDescription('Something went wrong with updating nft in your account. Try again later or inform the admin.');
        MSG.resetBot(channel);
        if (!queryres) {
            MSG.post(channel, errormsg);
            return false;
        }
        return true;
    }

    static removeRole(message, role) {

        const vrole = message.guild.roles.cache.get(role);
        message.member.roles.remove(vrole);
    }

};