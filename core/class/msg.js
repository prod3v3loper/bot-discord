/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

/**
 * MESSAGE HANDLER SYSTEM
 */
module.exports = class MSG {

    static post(channel, embed = {}, row, files) {

        if (!embed) return channel.send({ content: 'No embed' });

        if (row && files) return channel.send({ embeds: [embed], components: [row], files: [files] });
        if (row) return channel.send({ embeds: [embed], components: [row] });
        if (files) return channel.send({ embeds: [embed], files: [files] });

        return channel.send({ embeds: [embed] });
    }

    static resetBot(channel) {

        return channel.send('Bot Reload...').then(msg => { setTimeout(() => msg.delete(), 1000) });
    }
};
