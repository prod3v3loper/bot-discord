/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const {
    EmbedBuilder,
    AttachmentBuilder
} = require('discord.js');

const WARN = require('./warn.js');
const WALLET = require('./wallet.js');
const VERIFY = require('./verify.js');

const { createCanvas, Image } = require('@napi-rs/canvas');
const { readFile } = require('fs/promises');
const { request } = require('undici');

module.exports = class PROFILE {

    static async display(channel, message, user) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                const applyText = (canvas, text) => {
                    const context = canvas.getContext('2d');
                    let fontSize = 70;
                    do {
                        context.font = `${fontSize -= 10}px sans-serif`;
                    } while (context.measureText(text).width > canvas.width - 300);
                    return context.font;
                };

                const canvas = createCanvas(700, 250);
                const context = canvas.getContext('2d');

                const background = await readFile('./core/img/sahara.jpg');
                const backgroundImage = new Image();

                backgroundImage.src = background;
                context.drawImage(backgroundImage, 0, 0, canvas.width, canvas.height);

                context.strokeStyle = '#0099ff';
                context.strokeRect(0, 0, canvas.width, canvas.height);

                context.font = '28px sans-serif';
                context.fillStyle = '#ffffff';
                context.fillText('Profile', canvas.width / 2.5, canvas.height / 3.5);

                context.font = applyText(canvas, `${message.member.displayName}!`);
                context.fillStyle = '#ffffff';
                context.fillText(`${message.member.displayName}!`, canvas.width / 2.5, canvas.height / 1.8);

                context.beginPath();
                context.arc(125, 125, 100, 0, Math.PI * 2, true);
                context.closePath();
                context.clip();

                const { body } = await request(message.author.displayAvatarURL({ format: 'jpg' }));
                const avatar = new Image();

                avatar.src = Buffer.from(await body.arrayBuffer());
                context.drawImage(avatar, 25, 25, 200, 200);

                const attachment = new AttachmentBuilder(canvas.toBuffer('image/png'), { name: 'profile-image.png' });

                return message.reply({ files: [attachment], ephemeral: true });
            }
        }
    }

    static async card(channel, message, user) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                const userWarns = await WARN.read(message.author.id);

                const emb = new EmbedBuilder()
                    .setThumbnail(message.author.avatarURL({ format: 'jpg' }))
                    .setColor('#0000FF')
                    .setAuthor({
                        name: 'ID CARD',
                        iconURL: process.env.THUMB,
                        url: process.env.URL
                    })
                    .setTitle(`${message.member.displayName}`)
                    .setDescription(`Link: <@!` + message.author.id + `>`)
                    .addFields(
                        {
                            name: 'WARNS: ',
                            value: "```" + (userWarns ? userWarns.warns : 0) + "```",
                            inline: true,
                        },
                        {
                            name: 'COINS: ',
                            value: "```0```",
                            inline: true,
                        },
                        {
                            name: 'LVL: ',
                            value: "```0```",
                            inline: true,
                        },
                        {
                            name: 'Account created: ',
                            value: `<t:${parseInt(message.author.createdTimestamp / 1000, 10)}:R>`,
                            inline: true,
                        },
                        {
                            name: 'Member since: ',
                            value: `<t:${parseInt(message.member.joinedTimestamp / 1000, 10)}:R>`,
                            inline: true,
                        }
                    )
                    .setFooter({
                        text: process.env.FOOTERCOPY,
                        iconURL: process.env.FOOTERTHUMB
                    });

                return message.reply({ embeds: [emb], ephemeral: true });
            }
        }
    }
};