/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { token, guildId, clientId } = require('../../config.json');
const { EmbedBuilder } = require('discord.js');
const Warns = require('../../models/warn.js');
const MSG = require('./msg.js');

/**
 * WARN SYSTEM
 */
module.exports = class WARN {

  static async user(channel, message, words, bot) {

    // Define
    const user = message.mentions.users.first();
    const ch = message.mentions.channels.first();
    const msg = message.content.split(' ').slice(4).join(' ');

    // Checks
    if (!user) return message.reply({ content: 'No user mention was specifie for `!warn @user #channel warning-message`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    if (!ch) return message.reply({ content: 'No channel mention was specified for `!warn @user #channel warning-message`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    if (!msg) return message.reply({ content: 'No message was specified for `!warn @user #channel warning-message`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

    let uwarns = await Warns.findOne({
      where: {
        userId: user.id
      }
    });

    if (!uwarns) {
      uwarns = await Warns.create({
        userId: user.id,
        warns: 1
      });
    } else if (uwarns.warns != 3) {
      uwarns = await Warns.update({
        warns: Number(uwarns.warns) + Number(1)
      },
        {
          where: {
            userId: user.id
          }
        }
      );
    }

    uwarns = await Warns.findOne({
      where: {
        userId: user.id
      }
    });

    let warntext = '';
    if (uwarns.warns == 3) {
      warntext = '**You would be timout** <@!' + user.id + '>\n\n' + msg + '\n\nYou have already been warned ```' + uwarns.warns + '```\nYour timeout ends 60 seconds!\n\nPlease note our rules: <#' + process.env.RULESCHANNEL + '>';
      this.timeout(bot, user);
      await Warns.update({
        timeouts: Number(uwarns.timeouts) + 1
      },
        {
          where: {
            userId: user.id
          }
        }
      );
    } else {
      warntext = '**You would be warned** <@!' + user.id + '>\n\n' + msg + '\n\nYou have already been warned ```' + uwarns.warns + '```\n**After 3 times** you will be timeout!\n\nPlease note our rules: <#' + process.env.RULESCHANNEL + '>';
    }

    const errormsg = new EmbedBuilder()
      .setColor('#FF0000')
      .setTitle('Warning System')
      .setAuthor({
        name: 'SUPPORT WARNING ❗️',
        iconURL: process.env.THUMB,
        url: process.env.URL
      })
      .setDescription(warntext)
      .setFooter({
        text: process.env.FOOTERCOPY,
        iconURL: process.env.FOOTERTHUMB
      })
      .setTimestamp();

    const customCh = bot.channels.cache.get(ch.id);
    const mess = await MSG.post(customCh, errormsg);

    let msgIds = [];
    if (uwarns.msgIds != 'No Ids') {
      msgIds = JSON.parse(uwarns.msgIds);
      msgIds.push(mess.id);
    } else {
      msgIds.push(mess.id);
    }

    await Warns.update({
      msgIds: JSON.stringify(msgIds)
    },
      {
        where: {
          userId: user.id
        }
      }
    );

    return message.reply({ content: 'Your warning message was send successfully for user <@!' + user.id + '> and user was warned ' + uwarns.warns + ' times now! On 3 the user was timeout for 30 mins.' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
  }

  static async read(userId) {
    return await Warns.findOne({ where: { userId: userId } });
  }

  static async delete(message, bot) {

    const channel = message.mentions.channels.first();
    const user = message.mentions.users.first();
    if (!user) return message.reply({ content: 'No user mention was specified for `!warn remove @user #channel`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    if (!channel) return message.reply({ content: 'No channel mention was specified for `!warn remove @user #channel`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

    const tako = await this.read(user.id);
    const msgIds = JSON.parse(tako.msgIds);

    if (msgIds) {
      for (let i = 0; i < msgIds.length; i++) {
        const customCh = bot.channels.cache.get(channel.id);
        if (customCh && msgIds[i]) {
          const customChmsg = customCh.messages.cache.get(msgIds[i]);
          console.log(customChmsg);
          if (!customChmsg) continue;
          if (customChmsg) customChmsg.delete();
        }
      }
    }

    const queryres = await Warns.update(
      {
        warns: 0,
        msgIds: 'No Ids'
      },
      {
        where: {
          userId: user.id,
        }
      }
    );
    if (!queryres) return message.reply({ content: 'Something went wrong with deleting the user db warns!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

    message.delete(5000);
    return message.reply({ content: 'Warns and messages was successfully deleted for user <@!' + user.id + '>!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
  }

  static async kill(interaction) {

    const queryres = await Warns.destroy({ where: {}, truncate: true });
    if (!queryres) return interaction.reply({ content: 'Something went wrong with truncate the warns table!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

    return interaction.reply({ content: 'Warns table was truncate successfully!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
  }

  static async timeout(bot, user) {
    const guild = bot.guilds.cache.get(guildId);
    const usertimeout = guild.members.cache.get(user.id);
    if (!usertimeout || !user) return message.reply({ content: 'User <@!' + user.id + '> was not found to timeout!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

    usertimeout.timeout(60_000); // 1800 - 30 mins
  }

  static async endtimeout(bot, user) {
    const guild = bot.guilds.cache.get(guildId);
    const usertimeout = guild.members.cache.get(user.id);
    if (!usertimeout || !user) return message.reply({ content: 'User <@!' + user.id + '> was not found to end timeout!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    usertimeout.timeout(null);
  }

  static async ban(bot, user) {
    const guild = bot.guilds.cache.get(guildId);
    const userban = guild.members.cache.get(user.id);
    if (!userban || !user) return message.reply({ content: 'User <@!' + user.id + '> was not found to ban!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    userban.ban();
  }

  static async unban(bot, user) {
    const guild = bot.guilds.cache.get(guildId);
    const userunban = guild.members.cache.get(user.id);
    if (!userunban || !user) return message.reply({ content: 'User <@!' + user.id + '> was not found to unban!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    userunban.unban();
  }

  static async kick(bot, user) {
    const guild = bot.guilds.cache.get(guildId);
    const userkick = guild.members.cache.get(user.id);
    if (!userkick || !user) return message.reply({ content: 'User <@!' + user.id + '> was not found to kick!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    userkick.kick();
  }
};
