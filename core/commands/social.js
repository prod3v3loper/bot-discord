/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder().setName('social').setDescription('Replies with social info!'),
  async execute(interaction) {
    const embed = new EmbedBuilder()
      .setThumbnail(process.env.SOCIALTHUMB)
      .setAuthor({
        name: process.env.SOCIALAUTHORNAME,
        iconURL: process.env.SOCIALAUTHORTHUMB,
        url: process.env.SOCIALAUTHORURL
      })
      .setColor(process.env.SOCIALCOLOR)
      .setTitle(interaction.guild.name)
      .setDescription(`
**Official sites**
- Website: ` + process.env.WEBSITE + `

**Official NFT sites**
- WAX: ` + process.env.WAXLINK + `
- OpenSea: ` + process.env.OPENSEALINK + `

**Official social sites:**
- Twitter: ` + process.env.TWITTERLINK + `
- Instagram: ` + process.env.INSTAGRAMLINK + `

Discord: ` + process.env.DISCORDINVITELINK + ` (Invite link)`)
      .setFooter({
        text: process.env.FOOTERCOPY,
        iconURL: process.env.FOOTERTHUMB
      })
      .setTimestamp();
    try {
      await interaction.reply({ ephemeral: false, embeds: [embed] });
    } catch (error) {
      console.log(error);
    }
  },
};
