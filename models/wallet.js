/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

const { Sequelize } = require('sequelize');
const db = require('./db.js');

const Wallets = db.sequelize.define('wallets', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.TEXT,
  },
  gId: { type: Sequelize.INTEGER, defaultValue: 0 },
  verify: { type: Sequelize.INTEGER, defaultValue: 0 },
  wallet: { type: Sequelize.TEXT, defaultValue: 'No wallet' },
  nft: { type: Sequelize.INTEGER, defaultValue: 0 },
  nfts: { type: Sequelize.TEXT, defaultValue: 'No nfts' },
  wallets: { type: Sequelize.TEXT, defaultValue: 'No wallets' },
});

module.exports = Wallets;
