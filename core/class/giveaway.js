/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { token, guildId, clientId } = require('../../config.json');

const db = require('../../models/db.js');
const Giveaways = require('../../models/giveaways.js');
const Participate = require('../../models/participate.js');

const MSG = require('./msg.js');

let breakTimout = false;

const {
    ButtonStyle,
    ActionRowBuilder,
    ButtonBuilder,
    EmbedBuilder
} = require('discord.js');

/**
 * GIVEAWAY SYSTEM
 */
module.exports = class GIVEAWAY {

    static async create(args) {
        return await Giveaways.create({
            userId: args.userId,
            gId: args.guildId,
            mChId: args.mainChId,
            chId: args.channelId,
            mMsgId: args.mmsgId,
            msgId: args.msgId,
            winners: args.winners,
            price: args.price,
            end: args.end,
            duration: args.duration,
            ended: args.ended
        });
    }

    static async read(guildId) {
        return await Giveaways.findOne({
            where: {
                gId: guildId
            }
        });
    }

    static async update(id, data) {
        return await Giveaways.update(data, {
            where: { msgId: id }
        });
    }

    static delete(channel, words, bot) {
        const queryres = Giveaways.destroy({
            where: { msgId: words[2] }
        });
        if (!queryres) return channel.reply({ content: 'Something went wrong with deleting the giveaway!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        const fetchedChannel = bot.message.cache.get(words[2]);
        fetchedChannel.delete();
        return channel.reply({ content: 'Giveaway removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static async kill() {
        return await Giveaways.destroy({
            where: {},
            truncate: true,
        });
    }

    /**
     * Setup a giveaway
     * 
     * @usage !giveaway setup # 1k AKCE, 30m, 2
     * 
     * @param {*} message 
     * @param {*} bot 
     * 
     * @returns 
     */
    static async setup(channel, message, bot) {

        // Define
        let channelMention = message.mentions.channels.first();
        let userId = message.author.id;
        let args = message.content.split(" ").slice(3).join(" ").split(",");
        let price = args[0];
        let duration = args[1];
        let winners = args[2];

        // Checks
        if (!channelMention) return message.reply({ content: 'No channel mention was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (!price) return message.reply({ content: 'No price was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (!duration) return message.reply({ content: 'No duration/period was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (!winners) winners = 1;

        duration = duration.toLowerCase();

        if (duration.includes('s')) {
            duration = Number(duration.split('s')[0]) * 1000;
        } else if (duration.includes('m')) {
            duration = Number(duration.split('m')[0]) * 1000 * 60;
        } else if (duration.includes('h')) {
            duration = Number(duration.split('h')[0]) * 1000 * 60 * 60;
        } else if (duration.includes('d')) {
            duration = Number(duration.split('h')[0]) * 1000 * 60 * 60 * 24;
        } else {
            duration = Number(duration) * 1000 * 60;
        }

        let datetime = new Date().getTime();
        let time = Date.parse(new Date(datetime + duration)) / 1000;

        const embAdmin = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + price.toString())
            .setDescription("-----------------------------\nManage your giveaway with this dashboard!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">")
            .setFooter({
                text: 0 + ' Subscribers joined',
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();

        const buttonAdmin = new ButtonBuilder()
            .setCustomId('giveaway_admin_end')
            .setLabel('End early')
            .setEmoji('✅')
            .setStyle(ButtonStyle.Success);

        const buttonAdmin1 = new ButtonBuilder()
            .setCustomId('giveaway_admin_reroll')
            .setLabel('Reroll')
            .setEmoji('🆕')
            .setStyle(ButtonStyle.Primary);

        const buttonAdmin2 = new ButtonBuilder()
            .setCustomId('giveaway_admin_delete')
            .setLabel('Delete')
            .setEmoji('🗑️')
            .setStyle(ButtonStyle.Danger);

        const rowAdmin = new ActionRowBuilder()
            .addComponents(buttonAdmin)
            .addComponents(buttonAdmin1)
            .addComponents(buttonAdmin2);

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + price.toString())
            .setDescription("-----------------------------\nPlease click on the button to enter the competition!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">")
            .setFooter({
                text: 0 + ' Subscribers',
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();

        const button = new ButtonBuilder()
            .setCustomId('giveaway_participate')
            .setLabel('Subscribe')
            .setEmoji('✅')
            .setStyle(ButtonStyle.Success);

        const button1 = new ButtonBuilder()
            .setCustomId('giveaway_leave')
            .setLabel('Leave')
            .setEmoji('❌')
            .setStyle(ButtonStyle.Danger);

        const row = new ActionRowBuilder()
            .addComponents(button)
            .addComponents(button1);

        const guild = bot.guilds.cache.get(guildId);
        const ch = guild.channels.cache.get(channelMention.id);

        await MSG.post(ch, emb, row);
        let currentChannel = await MSG.post(channel, embAdmin, rowAdmin);

        if (currentChannel) {
            const args = {
                userId: userId,
                guildId: guildId,
                mainChId: channel.id,
                channelId: ch.id,
                mmsgId: currentChannel.id,
                msgId: ch.lastMessageId,
                price: price,
                winners: winners,
                end: (datetime + duration),
                duration: duration,
                ended: false
            };
            const rest = await this.create(args);
            if (rest) {
                breakTimout = setTimeout(async () => {
                    await GIVEAWAY.drawWinner(bot);
                }, 30000);
            }
        }
    }

    static async drawWinner(bot) {

        const events = await Giveaways.findAll();

        for (let i = 0; i < events.length; i++) {

            const rows = await this.findByMMsgId(events[i].mMsgId);

            if (!rows) return console.log('drawWinner !rows error!');

            const allrows = await Participate.count({
                where: {
                    msgId: rows.msgId
                }
            });

            const allrowsLast = await Participate.findOne({
                order: [['createdAt', 'DESC']]
            });

            const guild = bot.guilds.cache.get(guildId);

            const ch = guild.channels.cache.get(rows.chId);
            if (!ch) return console.log('drawWinner custom channel not found error!');
            const msg = await ch.messages.fetch(rows.msgId);
            if (!msg) return console.log('drawWinner custom message not found error!');

            const winner = await this.wins();

            msg.channel.send('Congrulations winners is drawn.').then(msg => { setTimeout(() => msg.delete(), 5000) });

            const emb = new EmbedBuilder()
                .setThumbnail(process.env.GIVEAWAYTHUMB)
                .setColor('#0000FF')
                .setAuthor({
                    name: 'GIVEAWAY 🎉',
                    iconURL: process.env.GIVEAWAYTHUMB,
                    url: process.env.GIVEAWAYURL
                })
                .setTitle('🎁 ' + rows.price)
                .setDescription("-----------------------------\nThe competition has ended and the winner is: " + (winner ? "\n🎉 <@!" + winner.userId + ">" : '\n❗️**No participates**') + "!\n\n**⏰ Giveaway end:** ENDED\n\n🛡️ Hosted by: <@!" + rows.userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
                .setFooter({
                    text: `${allrows} Subscribers`,
                    iconURL: process.env.GIVEAWAYTHUMB
                })
                .setTimestamp();
            msg.edit({ embeds: [emb] });

            const ch1 = bot.channels.cache.get(rows.mChId);
            if (!ch1) return console.log('drawWinner main channel not found error!');
            const msg1 = await ch1.messages.fetch(rows.mMsgId);
            if (!msg1) return console.log('drawWinner main message not found error!');

            const emb1 = new EmbedBuilder()
                .setThumbnail(process.env.GIVEAWAYTHUMB)
                .setColor('#0000FF')
                .setAuthor({
                    name: 'GIVEAWAY 🎉',
                    iconURL: process.env.GIVEAWAYTHUMB,
                    url: process.env.GIVEAWAYURL
                })
                .setTitle('🎁 ' + rows.price)
                .setDescription("-----------------------------\nThe competition has ended and the winner is: " + (winner ? "\n🎉 <@!" + winner.userId + ">" : '\n❗️**No participates**') + "!\n-----------------------------\n\n**⏰ Giveaway end:** ENDED\n\n🛡️ Hosted by: <@!" + rows.userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
                .setFooter({
                    text: `${allrows} Subscribers`,
                    iconURL: process.env.GIVEAWAYTHUMB
                })
                .setTimestamp();
            msg1.edit({ embeds: [emb1] });

            await this.update(rows.msgId, {
                end: 0,
                ended: true
            });
        }

        return console.log('drawWinner successfully!');
    }

    static async end(interaction, bot) {

        const rows = await this.findByMMsgId(interaction.message.id);

        clearTimeout(breakTimout);

        if (!rows) return interaction.reply({ content: 'No active giveaways events exists to ending!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.ended == true) return interaction.reply({ content: 'Giveaway event has already ended!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let allrows = await Participate.count({
            where: {
                msgId: rows.msgId
            }
        });

        let allrowsLast = await Participate.findOne({
            order: [['createdAt', 'DESC']]
        });

        let userId = rows.userId;

        const ch = await bot.channels.fetch(rows.chId);
        if (!ch) return interaction.reply({ content: 'Giveaway channel not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg = await ch.messages.fetch(rows.msgId);
        if (!msg) return interaction.reply({ content: 'Giveaway channel messsage not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let winner = await this.wins();

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nThe competition has ended and the winner is: \n" + (winner ? "\n🎉 <@!" + winner.userId + ">" : '\n❗️**No participates**') + "!\n\n**⏰ Giveaway end:** ENDED by ADMIN\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg.edit({ embeds: [emb] });

        const ch1 = bot.channels.cache.get(rows.mChId);
        const msg1 = ch1.messages.cache.get(interaction.message.id);

        const emb1 = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nThe competition has ended and the winner is: " + (winner ? "\n🎉 <@!" + winner.userId + ">" : '\n❗️**No participates**') + "!\n\n**⏰ Giveaway end:** ENDED by ADMIN\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg1.edit({ embeds: [emb1] });

        await this.update(rows.msgId, {
            end: 0,
            ended: true
        });

        return interaction.reply({ content: 'Giveaway event ended prematurely!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async reroll(interaction, bot) {

        let rows = await this.findByMMsgId(interaction.message.id);
        if (!rows) return interaction.reply({ content: 'No active giveaways exists to reroll!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.ended == false) return interaction.reply({ content: 'Event is already rerolled!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let datetime = new Date().getTime();
        let duration = Number(rows.duration);
        let time = Date.parse(new Date(datetime + duration)) / 1000;

        let allrows = await Participate.count({
            where: {
                msgId: rows.msgId
            }
        });

        let allrowsLast = await Participate.findOne({
            order: [['createdAt', 'DESC']]
        });

        let userId = rows.userId;

        const g = bot.guilds.cache.get(guildId);
        const ch = g.channels.cache.get(rows.chId);
        if (!ch) return interaction.reply({ content: 'Giveaway channel not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg = await ch.messages.fetch(rows.msgId);
        if (!msg) return interaction.reply({ content: 'Giveaway channel messsage not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nThe competition has **rerolled** and **new winner will be drawn**!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg.edit({ embeds: [emb] });

        const ch1 = g.channels.cache.get(rows.mChId);
        if (!ch1) return interaction.reply({ content: 'Giveaway channel 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg1 = await ch1.messages.fetch(rows.mMsgId);
        if (!msg1) return interaction.reply({ content: 'Giveaway channel messsage 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb1 = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nThe competition has **rerolled** and **new winner will be drawn**!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg1.edit({ embeds: [emb1] });

        const result = await this.update(rows.msgId, {
            end: (datetime + duration),
            ended: false
        });

        if (result) {
            breakTimout = setTimeout(async () => {
                await GIVEAWAY.drawWinner(bot);
            }, 30000);
        }

        return interaction.reply({ content: 'New winner will be drawn!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async participate(interaction, bot) {

        let rows = await this.findByMsgId(interaction.message.id);
        if (!rows) return interaction.reply({ content: 'No active giveaways exists to join!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.ended == true) return interaction.reply({ content: 'Giveaways has already ended!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let row = await Participate.findOne({
            where: {
                userId: interaction.user.id,
                msgId: interaction.message.id
            }
        });
        if (row) return interaction.reply({ content: 'You are already taking part in the competition!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        if (rows.end == true) return interaction.reply({ content: 'The sweepstakes has either expired and is awaiting a reroll or to be deleted!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let state = await Participate.create({
            userId: interaction.user.id,
            msgId: interaction.message.id
        });
        if (!state) return interaction.reply({ content: 'Something went wrong with participate the giveaway, please try again!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let allrows = await Participate.count({
            where: {
                msgId: interaction.message.id
            }
        });

        let time = Date.parse(new Date(rows.end)) / 1000;
        let userId = rows.userId;

        const guild = bot.guilds.cache.get(guildId);
        const ch = guild.channels.cache.get(rows.chId);
        if (!ch) return interaction.reply({ content: 'Giveaway channel not found!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg = ch.messages.cache.get(interaction.message.id);
        if (!msg) return interaction.reply({ content: 'Giveaway channel messsage not found!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nPlease click on the button to enter the competition!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">\n\nLast join: <@!" + interaction.user.id + ">")
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg.edit({ embeds: [emb] });

        const ch1 = guild.channels.cache.get(rows.mChId);
        if (!ch1) return interaction.reply({ content: 'Giveaway channel 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg1 = await ch1.messages.fetch(rows.mMsgId);
        if (!msg1) return interaction.reply({ content: 'Giveaway channel messsage 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb1 = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nPlease click on the button to enter the competition!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">\n\nLast join: <@!" + interaction.user.id + ">")
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg1.edit({ embeds: [emb1] });

        return interaction.reply({ content: 'You have successfully registered for the competition, good luck!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async leave(interaction, bot) {

        let rows = await this.findByMsgId(interaction.message.id);
        if (!rows) return interaction.reply({ content: 'No active giveaways exists to join!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.ended == true) return interaction.reply({ content: 'Giveaways has already ended!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let row = await Participate.findOne({
            where: {
                userId: interaction.user.id,
                msgId: interaction.message.id
            }
        });
        if (!row) return interaction.reply({ content: 'You are not in the competition!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        if (rows.end == true) return interaction.reply({ content: 'The sweepstakes has either expired and is awaiting a reroll or to be deleted!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let state = await Participate.destroy({
            where: {
                userId: interaction.user.id
            }
        });
        if (!state) return interaction.reply({ content: 'Something went wrong with participate the giveaway, please try again!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        let allrows = await Participate.count({
            where: {
                msgId: interaction.message.id
            }
        });

        let allrowsLast = await Participate.findOne({
            order: [['createdAt', 'DESC']]
        });

        let time = Date.parse(new Date(rows.end)) / 1000;
        let userId = rows.userId;

        const guild = bot.guilds.cache.get(guildId);
        const ch = guild.channels.cache.get(rows.chId);
        if (!ch) return interaction.reply({ content: 'Giveaway channel not found!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg = ch.messages.cache.get(interaction.message.id);
        if (!msg) return interaction.reply({ content: 'Giveaway channel messsage not found!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nPlease click on the button to enter the competition!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg.edit({ embeds: [emb] });

        const ch1 = guild.channels.cache.get(rows.mChId);
        if (!ch1) return interaction.reply({ content: 'Giveaway channel 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const msg1 = await ch1.messages.fetch(rows.mMsgId);
        if (!msg1) return interaction.reply({ content: 'Giveaway channel messsage 1 not found to edit!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const emb1 = new EmbedBuilder()
            .setThumbnail(process.env.GIVEAWAYTHUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'GIVEAWAY 🎉',
                iconURL: process.env.GIVEAWAYTHUMB,
                url: process.env.GIVEAWAYURL
            })
            .setTitle('🎁 ' + rows.price)
            .setDescription("-----------------------------\nPlease click on the button to enter the competition!\n\n**⏰ Giveaway end:** <t:" + time + ":R>\n\n🛡️ Hosted by: <@!" + userId + ">" + (allrowsLast ? "\n\nLast join: <@!" + allrowsLast.userId + ">" : ''))
            .setFooter({
                text: `${allrows} Subscribers`,
                iconURL: process.env.GIVEAWAYTHUMB
            })
            .setTimestamp();
        msg1.edit({ embeds: [emb1] });

        return interaction.reply({ content: 'You leave successfully the competition!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async remove(interaction, bot) {

        let rows = await this.findByMMsgId(interaction.message.id);
        if (!rows) return interaction.reply({ content: 'No active giveaways exists to delete!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.end != 0) return interaction.reply({ content: 'The giveaway was not ended and is active!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        if (rows.ended == false) return interaction.reply({ content: 'The giveaway was not ended and is active!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const guild = bot.guilds.cache.get(guildId);
        const chdelete = guild.channels.cache.get(rows.chId);
        const msgdelete = await chdelete.messages.fetch(rows.msgId);
        msgdelete.delete();

        const ch1delete = guild.channels.cache.get(rows.mChId);
        const msg1delete = await ch1delete.messages.fetch(rows.mMsgId);
        msg1delete.delete();

        let rowsdelete = await Giveaways.destroy({
            where: {
                mMsgId: interaction.message.id
            }
        });

        const allPart = await Participate.findAll();
        if (allPart.length > 0) {
            let rowdelete = await Participate.destroy({
                where: {
                    mMsgId: interaction.message.id
                }
            });

            if (!rowdelete) return interaction.reply({ content: 'Something went wrong with deleting the participate table, check your db and channels!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        }
        if (!rowsdelete) return interaction.reply({ content: 'Something went wrong with deleting the giveaway table, check your db and channels!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return interaction.reply({ content: 'Giveaway removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static async findByMsgId(msgId) {
        return await Giveaways.findOne({
            where: {
                msgId: msgId
            }
        });
    }

    static async findByMMsgId(msgId) {
        return await Giveaways.findOne({
            where: {
                mMsgId: msgId
            }
        });
    }

    static async last() {
        return await Giveaways.findOne({
            order: [['createdAt', 'DESC']]
        });
    }

    static async wins() {
        return await Participate.findOne({
            order: db.sequelize.random()
        });
    }

};