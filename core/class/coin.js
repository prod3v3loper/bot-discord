/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config({ override: true });

const { token, guildId, clientId } = require('../../config.json');
const { EmbedBuilder } = require('discord.js');

const Coins = require('../../models/coin.js');

const MSG = require('./msg.js');
const WALLET = require('./wallet.js');
const VERIFY = require('./verify.js');

/**
 * WARN SYSTEM -  CRUD
 */
module.exports = class Coin {

    static check(channel, userCoin) {

        if (!userCoin || userCoin == null) {
            const errormsg = new EmbedBuilder()
                .setColor('#FF0000')
                .setTitle('Ohhh!')
                .setDescription('No coins please add fisrt coins with typing `!coins`!');
            return MSG.post(channel, errormsg);
        }
        return true;
    }

    static async create(channel, message, user, userCoin) {

        if (!userCoin && userCoin == null) {
            const queryres = await Coins.create({
                userId: message.author.id,
                coin: 100,
                dailyAt: Date.now() + 86400000
            });
            const errormsg = new EmbedBuilder().setColor('#FF0000').setTitle('Ohhh!').setDescription('Something went wrong creating your coins. Try again later or inform the admin.');
            if (!queryres) return MSG.post(channel, errormsg);
        }
    }

    static async read(userId) {
        return await Coins.findOne({
            where: { userId: userId }
        });
    }

    static async update(channel, user, data) {

        // Define
        let userMention = message.mentions.users.first();

        // Checks
        if (!userMention) return channel.reply({ content: 'No user mention found!', ephemeral: true }).then((msg) => { setTimeout(() => msg.delete(), 5000) });

        const update = await Coins.update(data, { where: { userId: userMention.id } });

        if (!update) return channel.reply({ content: 'Something went wrong with deleting ' + address + ' wallet!', ephemeral: true }).then((msg) => { setTimeout(() => msg.delete(), 5000) });
        return channel.reply({ content: 'Wallet ' + address + ' removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => { setTimeout(() => msg.delete(), 5000) });
    }

    static delete(channel, message) {

        // Define
        let userMention = message.mentions.users.first();

        // checks
        if (!userMention) return message.reply({ content: 'No user mention was specified for `!coin remove @user`' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const queryres = Coins.destroy({ where: { userId: userMention.id } });

        if (!queryres) return channel.send({ content: 'Something went wrong with deleting the coin user!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        return channel.send({ content: 'Coin user removed successfully! :white_check_mark:', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async kill(interaction) {

        const queryres = await Coins.destroy({ where: {}, truncate: true });

        if (!queryres) return interaction.reply({ content: 'Something went wrong with truncate the coin table!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        return interaction.reply({ content: 'Coin table was truncate successfully!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static display(channel, message, user, userCoin, bot) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                if (!userCoin) {
                    this.create(channel, message, user, userCoin);
                }

                let OneDay = new Date().getTime();
                let userDate = 0;
                if (userCoin && userCoin.dailyAt != undefined) {
                    userDate = userCoin.dailyAt;
                }

                let difference = userDate - OneDay;
                let daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                difference -= daysDifference * 1000 * 60 * 60 * 24
                let hoursDifference = Math.floor(difference / 1000 / 60 / 60);
                difference -= hoursDifference * 1000 * 60 * 60
                let minutesDifference = Math.floor(difference / 1000 / 60);
                difference -= minutesDifference * 1000 * 60
                let secondsDifference = Math.floor(difference / 1000);

                const users = bot.users.cache.get(message.author.id);

                const emb = new EmbedBuilder()
                    .setColor('#0000FF')
                    .setThumbnail(process.env.COINIMAGE)
                    .setTitle('🪙 COINS')
                    .setDescription(
                        `${users}` + '\n\nThe ' + process.env.COIN + ' was the chief monetary unit of the Ottoman Empire, a silver coin.\n\nTo receive game benefits. <#' + process.env.GAMECHANNELID + '>\n\nAnd currently AKCE in your possession'
                    )
                    .addFields(
                        {
                            name: 'AKCE',
                            value: userCoin && userCoin.coin ? "```💵 " + userCoin.coin + ",00 $AKE```" : "```💵 0,00 $AKE```",
                            inline: true,
                        }
                    )
                    .addFields(
                        {
                            name: userDate == 0 ? 'DAILY NOT ACTIVATED' : 'NEXT DAILY',
                            value: userDate == 0 ? '```!daily```' : '```⏰ ' + hoursDifference + ' : ' + minutesDifference + ' : ' + secondsDifference + ' (24h period)```',
                            inline: true,
                        }
                    );
                return MSG.post(channel, emb);
            }
        }
    }

    static daily(channel, message, user, userCoin, bot) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                if (!userCoin) {
                    const nowallet = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle(`You don't have coins`)
                        .setDescription('Please add first coins with `!coins` to claim your dailys.\n');
                    return MSG.post(channel, nowallet);
                }

                let OneDay = new Date().getTime();
                let userDate = 0;
                if (userCoin.dailyAt != undefined) {
                    userDate = userCoin.dailyAt;
                }

                if (userDate == 0 || userDate != 0 && OneDay > userDate) {

                    let count = 10 + (userCoin.coin - 0);
                    const queryres = Coins.update({
                        coin: count,
                        dailyAt: Date.now() + 86400000
                    }, {
                        where: { userId: message.author.id }
                    });
                    const errormsg = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle('Ohhh!')
                        .setDescription('Something went wrong with adding your wallet. Try again later or inform the admin.');
                    if (!queryres) return MSG.post(channel, errormsg);

                    const user = bot.users.cache.get(message.author.id);

                    const emb = new EmbedBuilder()
                        .setColor('#0000FF')
                        .setTitle(`🪙 DAILY`)
                        .setThumbnail(process.env.COINIMAGE)
                        .setDescription(
                            `${user}` + '\n\nThe ' + process.env.COIN + ' was the chief monetary unit of the Ottoman Empire, a silver coin.\n\nTo receive game benefits. <#' + process.env.GAMECHANNELID + '>\n\nDaily claimed successfully 🎉'
                        )
                        .addFields(
                            {
                                name: 'AKCE: ',
                                value: count ? "```💵 " + count + ",00 $AKE```" : "```💵 0,00 $AKE```",
                                inline: true,
                            }
                        );
                    MSG.post(channel, emb);
                } else {

                    let difference = userDate - OneDay;
                    let daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                    difference -= daysDifference * 1000 * 60 * 60 * 24
                    let hoursDifference = Math.floor(difference / 1000 / 60 / 60);
                    difference -= hoursDifference * 1000 * 60 * 60
                    let minutesDifference = Math.floor(difference / 1000 / 60);
                    difference -= minutesDifference * 1000 * 60
                    let secondsDifference = Math.floor(difference / 1000);

                    const guild = bot.guilds.cache.get(guildId);
                    const user = guild.members.cache.get(message.author.id);

                    const emb = new EmbedBuilder()
                        .setColor('#0000FF')
                        .setTitle(`🪙 DAILY`)
                        .setThumbnail(process.env.COINIMAGE)
                        .setDescription(
                            `${user}` + '\n\nYour time to claim ' + process.env.COIN + ' coins again!\n\nTo receive game benefits. <#' + process.env.GAMECHANNELID + '>'
                        )
                        .addFields(
                            {
                                name: 'NEXT DAILY',
                                value: '```⏰ ' + hoursDifference + ' : ' + minutesDifference + ' : ' + secondsDifference + ' (24h period)```',
                                inline: true,
                            }
                        )
                        .addFields(
                            {
                                name: 'AKCE',
                                value: userCoin && userCoin.coin ? '```💵 ' + userCoin.coin + ',00 $AKE```' : '```💵 0,00 $AKE```',
                                inline: true,
                            }
                        );
                    MSG.post(channel, emb);
                }
            }
        }
    }

    static async getMax() {
        return await Coins.findAll({
            order: [
                ['coin', 'DESC']
            ]
        });
    }

};
