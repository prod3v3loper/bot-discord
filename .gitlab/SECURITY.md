# Security Policy

## Supported Versions

We only support the latest version.

| Version        | Supported          |
| -------------- | ------------------ |
| x.x.x latest   | :white_check_mark: |
| 5.0.x          | :x:                |
| 4.0.x          | :x:                |
| < 4.0          | :x:                |
