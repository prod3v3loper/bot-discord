/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config({ override: true });

const { EmbedBuilder } = require('discord.js');

const MSG = require('./msg.js');

module.exports = class NEWS {

    static async setup(message, bot) {

        let channelMention = message.mentions.channels.first();
        if (!channelMention) return message.channel.send({ content: 'Choose a channel and mention it, to post your news!' });

        this.send({
            color: Number(process.env.NEWSCOLOR),
            title: process.env.NEWSTITLE,
            description: process.env.NEWSDESC,
            thumbnail: process.env.NEWSTHUMB,
            url: process.env.NEWSURL,
            image: process.env.NEWSIMG,
            author: process.env.NEWSAUTHOR,
            authorUrl: process.env.NEWSAUTHORURL,
            authorIcon: process.env.NEWSAUTHORTHUMB,
            mainContentTitle: process.env.MAINTITLE,
            mainContentDesc: process.env.MAINDESC,
            field1title: process.env.MAINTITLE1,
            field1desc: process.env.MAINDESC1,
            field1inline: true,
            field2title: process.env.MAINTITLE2,
            field2desc: process.env.MAINDESC2,
            field2inline: true,
            field3title: process.env.MAINTITLE3,
            field3desc: process.env.MAINDESC3,
            field3inline: true,
            footer: process.env.NEWSFOOTERCOPY,
            footerIcon: process.env.NEWSFOOTERTHUMB
        }, bot, channelMention);
    }

    static send(data, bot, channelMention) {

        if (!data) return message.channel.send({ content: 'No data' });

        const Embed = new EmbedBuilder()
            .setColor(data.color)
            .setTitle(data.title)
            .setURL(data.url)
            .setAuthor({
                name: data.author,
                iconURL: data.authorIcon,
                url: data.authorUrl
            })
            .setDescription(data.description)
            .setThumbnail(data.thumbnail)
            .addFields(
                {
                    name: '\u200B',
                    value: '\u200B'
                },
                {
                    name: data.mainContentTitle,
                    value: data.mainContentDesc
                },
                {
                    name: '\u200B',
                    value: '\u200B'
                },
                {
                    name: data.field1title,
                    value: data.field1desc,
                    inline: data.field1inline
                },
                {
                    name: data.field2title,
                    value: data.field2desc,
                    inline: data.field2inline
                }
                // {
                //     name: data.field3title,
                //     value: data.field3desc,
                //     inline: data.field3inline
                // }
            )
            .setImage(data.image)
            .setTimestamp()
            .setFooter({
                text: data.footer,
                iconURL: data.footerIcon
            });

        const customChannel = bot.channels.cache.get(channelMention.id);
        MSG.post(customChannel, Embed);
    }

};