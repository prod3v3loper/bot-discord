/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 *
 * discord.js to build bot
 * @see https://discordjs.guide/creating-your-bot/
 */

// CONFIG
const { token, guildId, clientId } = require('../config.json');
require('dotenv').config({ override: true });

const path = require('path');
const fs = require('node:fs');

// DISCORD JS
const {
    Client,
    GatewayIntentBits,
    Collection,
    ChannelType,
    EmbedBuilder,
    Partials
} = require('discord.js');

const bot = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildPresences,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildMessageReactions,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildMessageTyping,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.DirectMessageReactions,
        GatewayIntentBits.DirectMessageTyping,
        GatewayIntentBits.MessageContent
    ],
    partials: [
        Partials.Channel,
        Partials.Message,
        Partials.Reaction
    ]
});

const VERIFY = require('../core/class/verify.js');
const WALLET = require('../core/class/wallet.js');
const XP = require('../core/class/xp.js');
const COIN = require('../core/class/coin.js');
const RANK = require('../core/class/rank.js');
const TICKET = require('../core/class/ticket.js');
const GAME = require('../core/class/game.js');
const GIVEAWAY = require('../core/class/giveaway.js');
const WARN = require('../core/class/warn.js');
const PROFILE = require('../core/class/profile.js');
const NEWS = require('../core/class/news.js');
const SUKIE = require('../core/class/sukie.js');
const LB = require('../core/class/leaderboard.js');
const SOCIAL = require('../core/class/social.js');
const MODAL = require('../core/class/modal.js');
const MYSQL = require('../core/class/mysql.js');

// 3600000
const myInterval = setInterval(() => {
    LB.autoupdate(bot);
}, 3600000);

/**
 * COMMAND HANDLING
 *
 * @see https://discordjs.guide/creating-your-bot/command-handling.html
 */
bot.commands = new Collection();
const commandFiles = fs.readdirSync(path.join(__dirname, '../core/commands')).filter((file) => file.endsWith('.js'));
for (const file of commandFiles) {
    const command = require(`../core/commands/${file}`);
    bot.commands.set(command.data.name, command);
}

bot.on("messageReactionAdd", async (reaction, user) => {
    await reaction.fetch();
    const { message, emoji } = reaction;
    await message.fetch();
    const channel = bot.channels.cache.get(message.channelId);
    if (channel.id == process.env.EXTRAROLECHANNELID) {
        if (emoji.name === "🎉") {
            SUKIE.gang(channel, message, emoji, bot, user);
        }
    }
});

bot.on("messageReactionRemove", async (reaction, user) => {
    if (user.bot) return false;
    await reaction.fetch();
    const { message, emoji } = reaction;
    await message.fetch();
    const channel = bot.channels.cache.get(message.channelId);
    if (channel.id == process.env.EXTRAROLECHANNELID) {
        // if (message.guild.member(user).roles.cache.find(r => r.name === "Verified")) {

        // }
    }
});

// On message
bot.on("messageCreate", async (message) => {

    // Check if bot
    if (message.author.bot) return false;

    // Get channel
    const channel = bot.channels.cache.get(message.channelId);
    // Get words
    const words = message.content.split(' ');

    // Get user datas
    const user = await WALLET.read(message.author.id);
    const userXp = await XP.read(message.author.id);
    const userCoin = await COIN.read(message.author.id);
    const userRank = await RANK.read(message.author.id);

    const userTickets = await TICKET.findallcount();

    if (channel.id == process.env.EXTRAROLE2CHANNELID) {
        if (message.content.startsWith('!sukie fam')) {
            SUKIE.fam(channel, message, bot);
        }
    }

    /************************************************************************* */

    /**
     * XP SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!level') && !words[1]) {
        XP.display(channel, message, user, userXp);
    }
    XP.update(channel, message, user, userXp);

    /************************************************************************* */

    /**
     * COIN SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!coins') && !words[1]) {
        COIN.display(channel, message, user, userCoin, bot);
    }

    /**
     * DAILY SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!daily') && !words[1]) {
        COIN.daily(channel, message, user, userCoin, bot);
    }

    /************************************************************************* */

    /**
     * SOCIAL SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!social twitter')) {
        SOCIAL.create(channel, message, words, 'twitter', userCoin);
    }

    /************************************************************************* */

    /**
     * RANKING SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!rank') && !words[1]) {
        RANK.display(channel, message, user, userRank);
    }

    if (message.content.startsWith('!buyrank list')) {
        RANK.list(channel, message, user, userRank);
    }

    if (message.content.startsWith('!buyrank') && words[1] && words[1] != 'list') {
        RANK.buy(channel, message, words, user, userCoin, userRank);
    }

    /************************************************************************* */

    /**
     * GAMING SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!flip') && words[1] && words[2]) {
        GAME.flip(channel, message, words, user, userCoin, bot);
    }

    /************************************************************************* */

    /**
     * PROFILE SYSTEM - ALL CAN FIRE
     */
    if (message.content.startsWith('!profile')) {
        PROFILE.display(channel, message, user);
    }

    if (message.content.startsWith('!card')) {
        PROFILE.card(channel, message, user);
    }

    /************************************************************************* */

    /**
     * ONLY THIS CHANNELS VERIFIYING - ALL CAN FIRE
     */
    if (message.channelId == process.env.VERIFYCHANNELID || message.channelId == process.env.VERIFYCHANNELID2) {
        /**
         *  SHOW WALLETS - Displays user wallets
         */
        if (message.content.startsWith('.wallets') && !words[1]) {
            WALLET.display(channel, message, user);
        }

        /**
         * ADD WALLET - Adds a wallet to a user
         */
        if (message.content.startsWith('.wallet add') && words[2] != '' && !words[3]) {
            WALLET.create(channel, message, user, words[2]);
        }

        /**
         * VERIFY - Verifying the user wallet
         */
        if (message.content.startsWith('.wallet verify') && !words[2]) {
            VERIFY.process(channel, message, user, bot);
        }

        /**
         * CHECK NFT - Checks if user have nfts in wallet
         */
        if (message.content.startsWith('.wallet nfts') && !words[2]) {
            VERIFY.nft(channel, message, user, bot);
        }
    }

    /************************************************************************* */

    /**
     * ONLY ADMIN AND MODERATOR CAN FIRE
     */
    if (message.author.id === process.env.ADMINID || message.author.id === process.env.MODERATORID) {

        /**
         * NEWS SYSTEM
         */
        if (message.content.startsWith('!news setup')) {
            NEWS.setup(message, bot);
        }

        if (message.content.startsWith('!board setup')) {
            LB.setup(message, bot);
        }

        if (message.content.startsWith('!board update')) {
            LB.update(message, bot);
        }

        if (message.content.startsWith('!sync mysql')) {
            MYSQL.snyc(bot);
        }

        /**
         * GIVEAWAY SYSTEM
         */
        if (message.content.startsWith('!giveaway setup')) {
            GIVEAWAY.setup(channel, message, bot);
        }

        /**
         * TICKET SYSTEM
         */
        if (message.content.startsWith('!ticket setup')) {
            TICKET.setup(channel, message, bot);
        }

        if (message.content.startsWith('!ticket remove')) {
            //..
        }

        if (message.content.startsWith('!modal setup')) {
            MODAL.create(message, bot);
        }

        // WHITELIST
        if (words[0] === '.whitelist' && !words[1]) {
            // const verifiedList = await Wallets.findAll();
            // console.log(verifiedList);
            const XpList = Xp.findAll();
            console.log(XpList);
        }

        // WARN USER
        if (message.content.startsWith('!warn setup')) {
            WARN.user(channel, message, words, bot);
        }

        // DELETE WARN
        if (message.content.startsWith('!warn remove')) {
            WARN.delete(message, bot);
        }

        // UPDATE WALLET
        if (message.content.startsWith('.wallet update')) {
            WALLET.update(channel, message, words[2], words[3]);
        }

        // DELETE WALLET
        if (message.content.startsWith('.wallet remove')) {
            WALLET.delete(channel, message, words[2]);
        }

        // DELETE WALLET
        if (message.content.startsWith('!coin remove')) {
            COIN.delete(channel, message);
        }

        // DELETE WALLET
        if (message.content.startsWith('!xp remove')) {
            XP.delete(channel, message);
        }
    }
});

bot.on("interactionCreate", async (interaction) => {

    if (interaction.user.bot) return false;

    const { commandName } = interaction;

    console.log(`Command ${commandName} was fired!`);

    if (commandName != 'help' && commandName != 'server' && commandName != 'social' && commandName != 'user') {

        const user = await WALLET.read(interaction.user.id);
        if (!user || user && user.verify != 1) return interaction.reply({ content: 'Please verify yourself first!', ephemeral: true }).then((msg) => { setTimeout(() => msg.delete(), 5000) });

        if (interaction.customId == 'wallet_show') {

            const adder = new EmbedBuilder()
                .setColor('#000000')
                .setAuthor({
                    name: 'WALLETS',
                    iconURL: process.env.THUMB,
                    url: process.env.URL
                })
                .setThumbnail(process.env.THUMB)
                .setTitle(interaction.guild.name)
                .setDescription(
                    `${interaction.user}\n\n`
                )
                .addFields(
                    {
                        name: 'Main',
                        value: user && user.wallet ? "```" + user.wallet + "```" : "```NO WALLET```",
                        inline: true,
                    },
                    {
                        name: 'Other',
                        value: "```Can you add later...```",
                        inline: true,
                    }
                )
                .setFooter({
                    text: process.env.FOOTERCOPY,
                    iconURL: process.env.FOOTERTHUMB
                })
                .setTimestamp();

            try {
                return interaction.reply({ embeds: [adder], ephemeral: true });
            } catch (e) {
                console.log(e);
                return false;
            }
        }


        if (interaction.customId == 'create_ticket') {
            TICKET.create(bot, interaction, ChannelType);
        }

        if (interaction.customId == 'kill_ticket') {
            TICKET.delete(bot, interaction);
        }

        if (interaction.customId == 'giveaway_participate') {
            GIVEAWAY.participate(interaction, bot);
        }

        if (interaction.customId == 'giveaway_leave') {
            GIVEAWAY.leave(interaction, bot);
        }

        if (interaction.user.id === process.env.ADMINID || interaction.user.id === process.env.MODERATORID) {

            if (interaction.customId == 'giveaway_admin_end') {
                GIVEAWAY.end(interaction, bot);
            }

            if (interaction.customId == 'giveaway_admin_reroll') {
                GIVEAWAY.reroll(interaction, bot);
            }

            if (interaction.customId == 'giveaway_admin_delete') {
                GIVEAWAY.remove(interaction, bot);
            }

            if (interaction.isButton()) {
                if (interaction.customId === 'verification-button') {
                    MODAL.display(interaction);
                }
            }
        }
    }

    // Is modal submit 
    // if (interaction.isModalSubmit()) {

    // }

    // User input
    // if (interaction.isChatInputCommand()) {

    // }

    // Is command
    if (interaction.isCommand()) {
        const command = bot.commands.get(interaction.commandName);
        if (!command) return;
        try {
            command.execute(interaction);
        } catch (error) {
            console.error(error);
        }
    }

    // const { commandName } = interaction;
    // if (commandName === 'react') {
    //   const message = await interaction.reply({ content: 'You can react with Unicode emojis!', fetchReply: true });
    //   message.react('😄');
    // }

});

/**
 * EVENT HANDLING
 *
 * @see https://discordjs.guide/creating-your-bot/event-handling.html
 */
const eventFiles = fs.readdirSync(path.join(__dirname, '../core/events')).filter((file) => file.endsWith('.js'));
for (const file of eventFiles) {
    const event = require(`../core/events/${file}`);
    if (event.once) {
        bot.once(event.name, (...args) => event.execute(bot, ...args));
    } else {
        bot.on(event.name, (...args) => event.execute(bot, ...args));
    }
}

bot.login(token);
