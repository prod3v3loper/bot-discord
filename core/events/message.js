/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const HELP = require('../class/help.js');
const AI = require('../class/ai.js');

module.exports = {
    name: 'messageCreate',
    once: false,
    async execute(bot, message) {

        if (message.author.bot) return false;

        console.log(`${message.author.tag} in #${message.channel.name} triggered an interaction.`);

        // Get channel
        const channel = bot.channels.cache.get(message.channelId);
        // Get words
        const words = message.content.split(' ');

        HELP.checkForHelp(channel, message, words);

        if (message.content.startsWith('!AI')) {
            AI.answers(message);
        }
    }
};