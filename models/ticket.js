/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

const { Sequelize } = require('sequelize');
const db = require('./db.js');

const Tickets = db.sequelize.define('tickets', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.TEXT,
  },
  gId: { type: Sequelize.INTEGER, defaultValue: 0 },
  chId: { type: Sequelize.TEXT },
  msgId: { type: Sequelize.TEXT },
  mId: { type: Sequelize.TEXT }
});

module.exports = Tickets;