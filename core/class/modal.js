/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const {
    ModalBuilder,
    EmbedBuilder,
    ActionRowBuilder,
    ButtonBuilder,
    ButtonStyle,
    TextInputBuilder,
    TextInputStyle
} = require('discord.js');

module.exports = class MODAL {

    static create(message, bot) {

        let channelMention = message.mentions.channels.first();
        if (!channelMention) return message.reply({ content: 'No channel mention was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const embed = new EmbedBuilder()
            .setThumbnail(process.env.THUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'WEBSITE LOGIN',
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`📧 Your Ticket ${message.author.tag}`)
            .setDescription("You can access your website data")
            .setFooter({
                text: process.env.FOOTERCOPY,
                iconURL: process.env.FOOTERTHUMB
            });

        const button = new ActionRowBuilder();
        button.addComponents(
            new ButtonBuilder()
                .setCustomId('verification-button')
                .setStyle(ButtonStyle.Primary)
                .setLabel('Login'),
        );

        const g = bot.guilds.cache.get(message.guild.id);
        const ch = g.channels.cache.get(channelMention.id);

        ch.send({
            embeds: [embed],
            components: [button],
        });
    }

    static async display(interaction) {

        // Create the modal
        const modal = new ModalBuilder()
            .setCustomId('contact')
            .setTitle('Contact');

        // Add components to modal

        // Create the text input components
        const favoriteColorInput = new TextInputBuilder()
            .setCustomId('favoriteColorInput')
            // The label is the prompt the user sees for this input
            .setLabel("What is your concern, what is it about?")
            // Short means only a single line of text
            .setStyle(TextInputStyle.Short);

        const hobbiesInput = new TextInputBuilder()
            .setCustomId('hobbiesInput')
            .setLabel("Please describe the problem!")
            // Paragraph means multiple lines of text.
            .setStyle(TextInputStyle.Paragraph);

        // An action row only holds one text input,
        // so you need one action row per text input.
        const firstActionRow = new ActionRowBuilder().addComponents(favoriteColorInput);
        const secondActionRow = new ActionRowBuilder().addComponents(hobbiesInput);

        // Add inputs to the modal
        modal.addComponents(firstActionRow, secondActionRow);

        // Show the modal to the user
        await interaction.showModal(modal);
    }
};