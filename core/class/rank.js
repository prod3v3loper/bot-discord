/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { EmbedBuilder } = require('discord.js');

const Ranks = require('../../models/rank.js');
const Coins = require('../../models/coin.js');

const MSG = require('./msg.js');
const WALLET = require('./wallet.js');
const VERIFY = require('./verify.js');

/**
 * RANK SYSTEM - CRUD
 */
module.exports = class RANK {

    static async create(message) {
        return await Ranks.create({
            userId: message.author.id,
            rank: 'No Rank',
            rank1: 'No Rank',
            rank2: 'No Rank',
        });
    }

    static async read(userId) {
        return await Ranks.findOne({ where: { userId: userId } });
    }

    static async update(userId, data) {
        return await Ranks.update(data, {
            where: {
                userId: userId
            }
        });
    }

    static delete(channel, userID) {
        const queryres = Ranks.destroy({
            where: { userId: userID }
        });
        if (!queryres) return channel.reply({ content: 'Something went wrong with deleting the xp!', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
        return channel.reply({ content: 'Xp removed successfully! :white_check_mark:', ephemeral: true }).then((msg) => msg.delete({ timeout: '5000' }));
    }

    static async kill() {
        return await Ranks.destroy({
            where: {},
            truncate: true,
        });
    }

    static display(channel, message, user, userRank) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                if (!userRank) {
                    this.create(message);
                }

                const emb = new EmbedBuilder()
                    .setColor('#0000FF')
                    .setThumbnail(process.env.THUMB)
                    .setTitle('Rank')
                    .setDescription(
                        'The ranks allow you to open new channels with mor insides.\n\nTo see available ranks type: `!buyrank list`\n\nAnd ranks currently in your possession'
                    )
                    .addFields(
                        {
                            name: 'JANISSARY',
                            value: userRank && userRank.rank ? "```" + userRank.rank + "```" : "```NO RANK```",
                            inline: true,
                        },
                        {
                            name: 'AMIR',
                            value: userRank && userRank.rank1 ? "```" + userRank.rank1 + "```" : "```NO RANK```",
                            inline: true,
                        },
                        {
                            name: 'SULTAN',
                            value: userRank && userRank.rank2 ? "```" + userRank.rank2 + "```" : "```NO RANK```",
                            inline: true,
                        }
                    );
                return MSG.post(channel, emb);
            }
        }
    }

    static async list(channel, message, user, userRank) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                if (!userRank) {
                    this.create(message);
                }

                const emb = new EmbedBuilder()
                    .setColor('#0000FF')
                    .setThumbnail(process.env.THUMB)
                    .setTitle('Ranks you can buy')
                    .setDescription(
                        'The ranks, get higher rank. With higher ranks you unlock extra channels. You can buy this with our **$AKE** coins.\n\nTo receive game benefits or claim dailys to collect more **$AKE**:\n<#' + process.env.GAMECHANNELID + '>\n\nBuy your rank with `!buyrank rank-you-want-buy`.'
                    )
                    .addFields(
                        {
                            name: 'SULTAN',
                            value: "```15000 💵 $AKE```",
                            inline: true,
                        },
                        {
                            name: 'AMIR',
                            value: "```25000 💵 $AKE```",
                            inline: true,
                        },
                        {
                            name: 'JANISSARY',
                            value: "```50000 💵 $AKE```",
                            inline: true,
                        }
                    );
                MSG.post(channel, emb);
            }
        }
    }

    static async coinUpdate(message, count) {
        return await Coins.update({
            coin: count
        }, {
            where: {
                userId: message.author.id
            }
        });
    }

    static buy(channel, message, words, user, userCoin, userRank) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                if (!userRank) {
                    this.create(message);
                }

                if (!userCoin) {
                    const errormsg = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle('Ohhh!')
                        .setDescription('No coins please add fisrt coins with typing `!coins`!');
                    return MSG.post(channel, errormsg);
                }

                if (userCoin.coin <= 0) {
                    const errormsg = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle('Ohhh!')
                        .setDescription('You have not enough coins ! Claim your daily or get other way $AKE');
                    return MSG.post(channel, errormsg);
                }

                if (rolesMention.name == 'JANISSARY' || words[1] == 'JANISSARY') {

                    if (userCoin.coin < '5000') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have not enough money!');
                        return MSG.post(channel, errormsg);
                    }

                    if (userRank && userRank.rank == 'Janissary') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have the rank Janissary already!');
                        return MSG.post(channel, errormsg);
                    }

                    let newcoins = Number(userCoin.coin) - Number(5000);
                    if (this.coinUpdate(message, newcoins)) {
                        if (this.update(message.author.id, {
                            rank: 'Janissary'
                        })) {
                            const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID3);
                            message.member.roles.add(role);

                            const errormsg = new EmbedBuilder()
                                .setColor('#00FF00')
                                .setTitle('Rank')
                                .setDescription('You have received the JANISSARY rank, conrulations 🎉');
                            return MSG.post(channel, errormsg);
                        }
                    }

                }

                if (rolesMention.name == 'AMIR' || words[1] == 'AMIR') {

                    if (userCoin.coin < '25000') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have not enough money!');
                        return MSG.post(channel, errormsg);
                    }

                    if (userRank && userRank.rank1 == 'Amir') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have the rank Amir already!');
                        return MSG.post(channel, errormsg);
                    }

                    let newcoins = Number(userCoin.coin) - Number(25000);
                    if (this.coinUpdate(message, newcoins)) {
                        if (this.update(message.author.id, {
                            rank1: 'Amir'
                        })) {
                            const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID2);
                            message.member.roles.add(role);

                            const errormsg = new EmbedBuilder()
                                .setColor('#00FF00')
                                .setTitle('Rank')
                                .setDescription('You have received the AMIR rank, conrulations 🎉');
                            return MSG.post(channel, errormsg);
                        }
                    }

                }

                if (rolesMention.name == 'SULTAN' || words[1] == 'SULTAN') {

                    if (userCoin.coin < '50000') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have not enough money!');
                        return MSG.post(channel, errormsg);
                    }

                    if (userRank && userRank.rank2 == 'Sultan') {
                        const errormsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setDescription('You have the rank Sultan already!');
                        return MSG.post(channel, errormsg);
                    }

                    let newcoins = Number(userCoin.coin) - Number(50000);
                    if (this.coinUpdate(message, newcoins)) {
                        if (this.update(message.author.id, {
                            rank2: 'Sultan'
                        })) {
                            const role = message.guild.roles.cache.get(process.env.CUSTOMROLEIID1);
                            message.member.roles.add(role);

                            const errormsg = new EmbedBuilder()
                                .setColor('#00FF00')
                                .setTitle('Rank')
                                .setDescription('You have received the SULTAN rank, conrulations 🎉');
                            return MSG.post(channel, errormsg);
                        }
                    }
                }
            }
        }

    }
};