/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

module.exports = {
    name: 'unhandledRejection',
    once: false,
    execute(bot, error) {
        console.error('Unhandled promise rejection:', error);
    }
};