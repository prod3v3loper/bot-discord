/**
 * DISCORD BOT SQLite3 by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

module.exports = {
  test: {
    username: 'username_test',
    password: 'password_test',
    database: 'database_test',
    host: '127.0.0.1',
    port: 3306,
    dialect: 'sqlite',
    logging: false,
    storage: 'test.sqlite'
  },
  dev: {
    username: 'username_dev',
    password: 'password_dev',
    database: 'database_dev',
    host: '127.0.0.1',
    port: 3306,
    dialect: 'sqlite',
    logging: false,
    storage: 'dev.sqlite'
  },
  prod: {
    username: process.env.DBUSER,
    password: process.env.DBPASS,
    database: process.env.DBNAME,
    host: process.env.DBHOST,
    port: process.env.DBPORT,
    dialect: 'sqlite',
    logging: false,
    storage: 'database.sqlite'
  }
};
