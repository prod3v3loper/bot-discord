/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config({ override: true });

const { EmbedBuilder } = require('discord.js');

const Coins = require('../../models/coin.js');

const MSG = require('./msg.js');
const WALLET = require('./wallet.js');
const VERIFY = require('./verify.js');

/**
 * MINI GAME SYSTEM
 */
module.exports = class GAME {

    static flip(channel, message, words, user, userCoin, bot) {

        if (WALLET.check(channel, user, true)) {
            if (VERIFY.check(channel, user, true)) {

                const users = bot.users.cache.get(message.author.id);

                if (!userCoin) {
                    const errormsg = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setThumbnail(process.env.COINIMAGE)
                        .setTitle('Ohhh!')
                        .setDescription(`${users}` + '\n\nNo coins, please add first coins with typing `!coins`');
                    return MSG.post(channel, errormsg);
                }

                if (userCoin.coin <= 0) {
                    const errormsg = new EmbedBuilder()
                        .setColor('#FF0000')
                        .setTitle('Ohhh!')
                        .setThumbnail(process.env.COINIMAGE)
                        .setDescription(`${users}` + '\n\nYou have not enough coins ! Claim your daily or get other way $AKE');
                    return MSG.post(channel, errormsg);
                }

                var coinFlip = Math.random() < 0.3;
                var choice = words[2];
                let count = 0;

                if (coinFlip == 1) {
                    var flipResult = "head";
                } else {
                    var flipResult = "tail";
                }

                if (flipResult == choice) {
                    if (flipResult == "head") {
                        count = Number(userCoin.coin) + Number(words[1]);
                        if (count < 0) {
                            count = 0;
                        }
                        this.update(message, count);
                        const gamemsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setThumbnail(process.env.COINIMAGE)
                            .setDescription(`${users}` + '\n\n✅ YOU WON\n\nThe flip was head and you choose ' + choice + '...')
                            .addFields(
                                {
                                    name: 'Your committment',
                                    value: '```💵 ' + words[1] + ',00 $AKE```',
                                    inline: true,
                                },
                                {
                                    name: 'You have now',
                                    value: '```💵 ' + count + ',00 $AKE```',
                                    inline: true,
                                }
                            );
                        return MSG.post(channel, gamemsg);
                    } else {
                        count = Number(userCoin.coin) - Number(words[1]);
                        if (count < 0) {
                            count = 0;
                        }
                        this.update(message, count);
                        const gamemsg = new EmbedBuilder()
                            .setColor('#FF0000')
                            .setTitle('Ohhh!')
                            .setThumbnail(process.env.COINIMAGE)
                            .setDescription(`${users}` + '\n\n❌ YOU LOSE\n\nThe flip was tail and you choose ' + choice + '...')
                            .addFields(
                                {
                                    name: 'Your committment',
                                    value: '```💵 ' + words[1] + ',00 $AKE```',
                                    inline: true,
                                },
                                {
                                    name: 'You have now',
                                    value: '```💵 ' + count + ',00 $AKE```',
                                    inline: true,
                                }
                            );
                        return MSG.post(channel, gamemsg);
                    }
                } else {
                    if (flipResult == "head") {
                        count = Number(userCoin.coin) + Number(words[1]);
                        if (count < 0) {
                            count = 0;
                        }
                        this.update(message, count);
                        const gamemsg = new EmbedBuilder()
                            .setColor('#00FF00')
                            .setTitle('Yeah!')
                            .setThumbnail(process.env.COINIMAGE)
                            .setDescription(`${users}` + '\n\n✅ YOU WON\n\nThe flip was head and you choose ' + choice + '...')
                            .addFields(
                                {
                                    name: 'Your committment',
                                    value: '```💵 ' + words[1] + ',00 $AKE```',
                                    inline: true,
                                },
                                {
                                    name: 'You have now',
                                    value: '```💵 ' + count + ',00 $AKE```',
                                    inline: true,
                                }
                            );
                        return MSG.post(channel, gamemsg);
                    } else {
                        count = Number(userCoin.coin) - Number(words[1]);
                        if (count < 0) {
                            count = 0;
                        }
                        this.update(message, count);
                        const gamemsg = new EmbedBuilder()
                            .setColor('#FF0000')
                            .setTitle('Ohhh!')
                            .setThumbnail(process.env.COINIMAGE)
                            .setDescription(`${users}` + '\n\n❌ YOU LOSE\n\nThe flip was tail and you choose ' + choice + '...')
                            .addFields(
                                {
                                    name: 'Your committment',
                                    value: '```💵 ' + words[1] + ',00 $AKE```',
                                    inline: true,
                                },
                                {
                                    name: 'You have now',
                                    value: '```💵 ' + count + ',00 $AKE```',
                                    inline: true,
                                }
                            );
                        return MSG.post(channel, gamemsg);
                    }
                }
            }
        }
    }

    static update(message, count) {

        Coins.update({ coin: count }, { where: { userId: message.author.id } });
    }
};