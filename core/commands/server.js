/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('server')
    .setDescription('Replies with server info!'),
  async execute(interaction) {

    const embed = new EmbedBuilder()
      .setColor(process.env.SERVERCOLOR)
      .setThumbnail(process.env.SERVERTHUMB)
      .setAuthor({
        name: process.env.SERVERAUTHORNAME,
        iconURL: process.env.SERVERAUTHORTHUMB,
        url: process.env.SERVERAUTHORURL
      })
      .setTitle(interaction.guild.name)
      .setDescription(`${interaction.guild.name} community server data:`).addFields(
        {
          name: 'Members',
          value: interaction.guild.memberCount ? "```🏢 " + interaction.guild.memberCount + "```" : "```0```",
          inline: true,
        },
        {
          name: 'Verification Level',
          value: interaction.guild.verificationLevel ? "```🛡️ " + interaction.guild.verificationLevel + "```" : "```0```",
          inline: true,
        }
      )
      .setFooter({
        text: process.env.FOOTERCOPY,
        iconURL: process.env.FOOTERTHUMB
      })
      .setTimestamp();
    try {
      await interaction.reply({ ephemeral: false, embeds: [embed] });
    } catch (error) {
      console.log(error);
    }
  }
};
