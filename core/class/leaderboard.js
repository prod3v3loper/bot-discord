/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { EmbedBuilder } = require('discord.js');

const Leaderboard = require('../../models/lb.js');

const MSG = require('./msg.js');
const COIN = require('./coin.js');
const XP = require('./xp.js');

module.exports = class LB {

    static create() {

    }

    static read() {

    }

    static async autoupdate(bot) {

        const all = await Leaderboard.findAll();

        for (let key = 0; key < all.length; key++) {

            const maxuser = await COIN.getMax();
            const usermax = await bot.users.fetch(maxuser[0].userId);

            const emb = new EmbedBuilder()
                .setThumbnail(process.env.THUMB)
                .setColor('#0000FF')
                .setAuthor({
                    name: '👑 LEADERBOARD',
                    iconURL: process.env.THUMB,
                    url: process.env.URL
                })
                .setTitle(`First place is ` + usermax.username)
                .setDescription(`To all others good luck and have fun!`)
                .setFooter({
                    text: process.env.FOOTERCOPY,
                    iconURL: process.env.FOOTERTHUMB
                })
                .setTimestamp();

            let counter = 1;
            for (let i = 0; i < maxuser.length; i++) {

                const user = bot.users.cache.get(maxuser[i].userId);
                if (user) {

                    let userXp = await XP.read(user.id);

                    emb.addFields(
                        {
                            name: '---------------------',
                            value: '**' + counter + '** <@!' + user.id + '>',
                            inline: true,
                        },
                        {
                            name: '---------------------',
                            value: '```' + (userXp ? userXp.xp : 0) + ' XP```',
                            inline: true,
                        },
                        {
                            name: '---------------------',
                            value: '```' + (maxuser[i].coin ? maxuser[i].coin : 0) + ' 💵 $AKE```',
                            inline: true,
                        }
                    );
                    counter++;
                }
            }

            // Post in there our message with close button
            const ch = bot.channels.cache.get(all[key].chId);
            if (!ch) return console.log('No channel');
            const msg = await ch.messages.fetch(all[key].msgId);
            if (!msg) return console.log('No message');
            msg.edit({ embeds: [emb] });
        }
    }

    static async update(message, bot) {

        let channelMention = message.mentions.channels.first();
        if (!channelMention) return message.reply({ content: 'No channel mention was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const all = await Leaderboard.findOne({ where: { chId: channelMention.id } });

        if (!all) return message.reply({ content: 'No leaderboard to update!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const maxuser = await COIN.getMax();
        const usermax = bot.users.cache.get(maxuser[0].userId);

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.THUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: '👑 LEADERBOARD',
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`First place is ` + usermax.username)
            .setDescription(`To all others good luck and have fun!`)
            .setFooter({
                text: process.env.FOOTERCOPY,
                iconURL: process.env.FOOTERTHUMB
            })
            .setTimestamp();

        let counter = 1;
        for (let i = 0; i < maxuser.length; i++) {

            const user = bot.users.cache.get(maxuser[i].userId);
            if (user) {

                let userXp = await XP.read(user.id);

                emb.addFields(
                    {
                        name: '---------------------',
                        value: '**' + counter + '** <@!' + user.id + '>',
                        inline: true,
                    },
                    {
                        name: '---------------------',
                        value: '```' + (userXp ? userXp.xp : 0) + ' XP```',
                        inline: true,
                    },
                    {
                        name: '---------------------',
                        value: '```' + (maxuser[i].coin ? maxuser[i].coin : 0) + ' 💵 $AKE```',
                        inline: true,
                    }
                );
                counter++;
            }
        }

        // Post in there our message with close button
        const ch = bot.channels.cache.get(all.chId);
        const msg = await ch.messages.fetch(all.msgId);
        if (!msg) return message.channel.send({ content: 'Message not found!' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        msg.edit({ embeds: [emb] });

        await Leaderboard.update(
            {
                userId: message.author.id,
            },
            { where: { id: all.id } }
        );

        // Inform user 
        return message.reply({ content: 'Your leaderboard was successfully updated here: <#' + channelMention.id + '>', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static delete() {

    }

    static async setup(message, bot) {

        let channelMention = message.mentions.channels.first();

        if (!channelMention) return message.reply({ content: 'No channel mention was specified' }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const maxuser = await COIN.getMax();
        if (!maxuser) return message.reply({ content: 'No users have coin accounts' }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const usermax = bot.users.cache.get(maxuser[0].userId);

        const g = bot.guilds.cache.get(message.guild.id);

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.THUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: '👑 LEADERBOARD',
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`First place is ` + usermax.username)
            .setDescription(`To all others good luck and have fun!`)
            .setFooter({
                text: process.env.FOOTERCOPY,
                iconURL: process.env.FOOTERTHUMB
            })
            .setTimestamp();


        let counter = 1;
        for (let i = 0; i < maxuser.length; i++) {

            // Get created guild
            const user = bot.users.cache.get(maxuser[i].userId);
            if (user) {

                let userXp = await XP.read(user.id);

                emb.addFields(
                    {
                        name: '---------------------',
                        value: '**' + counter + '** <@!' + user.id + '>',
                        inline: true,
                    },
                    {
                        name: '---------------------',
                        value: '```' + (userXp ? userXp.xp : 0) + ' XP```',
                        inline: true,
                    },
                    {
                        name: '---------------------',
                        value: '```' + (maxuser[i].coin ? maxuser[i].coin : 0) + ' 💵 $AKE```',
                        inline: true,
                    }
                );
                counter++;
            }
        }

        // Get created ticket channel
        const ch = g.channels.cache.get(channelMention.id);
        // Post in there our message with close button
        await MSG.post(ch, emb);

        await Leaderboard.create({
            userId: message.author.id,
            gId: message.guild.id,
            chId: channelMention.id,
            msgId: ch.lastMessageId
        });

        // Inform user 
        return message.reply({ content: 'Your leaderboard was successfully created here: <#' + channelMention.id + '>', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static display() {

    }
};