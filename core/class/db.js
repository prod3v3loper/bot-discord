/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/getting-started/#connecting-to-a-database
 */

require('dotenv').config({ override: true });

const { Sequelize, Op, Model, DataTypes } = require('sequelize');

/**
 * DATABASE SQLITE
 *
 * https://discordjs.guide/sequelize/
 */
const sequelize = new Sequelize(process.env.DBNAME, process.env.DBUSER, process.env.DBPASS, {
  host: 'localhost',
  dialect: 'sqlite',
  logging: false,
  storage: 'database.sqlite'
});

try {
  sequelize.authenticate();
  console.log('DB sequelize connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

module.exports = sequelize;
