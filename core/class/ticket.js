/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { token, guildId, clientId } = require('../../config.json');

// DISCORD JS
const {
    ButtonStyle,
    ActionRowBuilder,
    PermissionsBitField,
    ButtonBuilder,
    EmbedBuilder
} = require('discord.js');

// Model
const Tickets = require('../../models/ticket.js');

// CLass
const MSG = require('./msg.js');

/**
 * TICKET SYSTEM
 */
module.exports = class TICKET {

    static async create(bot, interaction, ChannelType) {

        // Define
        let userData = await this.read(interaction.user.id);

        // CHecks
        if (userData) return interaction.reply({ content: 'You have created a ticket already!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        const { count } = await Tickets.findAndCountAll();

        // Create guild ticket channel 
        let result = await interaction.guild.channels.create({
            name: 'Ticket-' + count,
            type: ChannelType.GuildText,
            parent: process.env.TICKETCHANNELID,
            permissionOverwrites: [
                { id: interaction.guild.id, deny: [PermissionsBitField.Flags.ViewChannel] },
                { id: interaction.user.id, allow: [PermissionsBitField.Flags.ViewChannel, PermissionsBitField.Flags.SendMessages, PermissionsBitField.Flags.ReadMessageHistory] },
                { id: process.env.MODERATORID, allow: [PermissionsBitField.Flags.ViewChannel, PermissionsBitField.Flags.SendMessages, PermissionsBitField.Flags.ReadMessageHistory] },
                { id: bot.user.id, allow: [PermissionsBitField.Flags.ViewChannel, PermissionsBitField.Flags.SendMessages, PermissionsBitField.Flags.ReadMessageHistory] }
            ]
        });

        if (!result) return interaction.reply({ content: 'Something went wrong with creating your ticket! Try again later or inform the admin.', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        // Insert in db
        const queryres = await Tickets.create({
            userId: interaction.user.id,
            chId: result.id,
            gId: interaction.guild.id,
            mId: process.env.MODERATORID,
        });

        if (!queryres) return interaction.reply({ content: 'Something went wrong with creating your ticket! Try again later or inform the admin.', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });

        // Create embed
        const emb = new EmbedBuilder()
            .setThumbnail(process.env.THUMB)
            .setColor('#0000FF')
            .setAuthor({
                name: 'SUPPORT TICKET',
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setTitle(`📧 Your Ticket ${interaction.user.tag}`)
            .setDescription("\nPlease ask your question. If your request has been answered or your request has been resolved, please close your ticket.\n\nThanks")
            .setFooter({
                text: process.env.FOOTERCOPY,
                iconURL: process.env.FOOTERTHUMB
            });

        const button = new ButtonBuilder()
            .setCustomId('kill_ticket')
            .setLabel('Close ticket')
            .setEmoji('❌')
            .setStyle(ButtonStyle.Danger);

        const row = new ActionRowBuilder()
            .addComponents(button);

        // Get created guild
        const g = bot.guilds.cache.get(interaction.guild.id);
        // Get created ticket channel
        const ch = g.channels.cache.get(result.id);
        // Post in there our message with close button
        if (ch) MSG.post(ch, emb, row);
        // Inform user 
        return interaction.reply({ content: 'Your ticket was successfully created here: <#' + result.id + '>', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static async read(userId) {
        return await Tickets.findOne({ where: { userId: userId } });
    }

    static async findallcount() {
        return await Tickets.findAndCountAll();
    }

    static async update(userId) {
        return await Tickets.update({ where: { userId: userId } });
    }

    static async delete(bot, interaction) {
        const queryres = await Tickets.destroy({ where: { chId: interaction.channelId } });
        if (!queryres) return interaction.reply({ content: 'Something went wrong with deleting the support ticket!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        const guild = bot.guilds.cache.get(guildId);
        const fetchedChannel = guild.channels.cache.get(interaction.channelId);
        const state = fetchedChannel.delete();
        if (!state) return interaction.reply({ content: 'Something went wrong with deleting the support channel!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

    static setup(channel, message, bot) {

        // Define
        let mentionfirst = message.mentions.channels.first();

        // Checks
        if (!mentionfirst) return message.channel.send({ content: "Please set a categoriy channel to place the Ticket form." });

        const emb = new EmbedBuilder()
            .setThumbnail(process.env.THUMB)
            .setColor('#0000FF')
            .setTitle('📧 Tickets')
            .setAuthor({
                name: 'SUPPORT',
                iconURL: process.env.THUMB,
                url: process.env.URL
            })
            .setDescription("You can create tickets if you have **questions**, **need help** or if **something doesn't work**.\n\nWe are constantly striving to offer our community the best. Please be patient if a ticket cannot be processed immediately.\n\nCreate your ticket and ask your question in the ticket so we can answer it.\n\nPress **Create support ticket**")
            .setFooter({
                text: process.env.FOOTERCOPY,
                iconURL: process.env.FOOTERTHUMB
            });

        const button = new ButtonBuilder()
            .setCustomId('create_ticket')
            .setLabel('Create support ticket')
            .setEmoji('📧')
            .setStyle(ButtonStyle.Primary);

        const row = new ActionRowBuilder()
            .addComponents(button);

        // Get created guild
        const g = bot.guilds.cache.get(message.guild.id);
        // Get created ticket channel
        const ch = g.channels.cache.get(mentionfirst.id);

        if (ch) MSG.post(ch, emb, row);
    }

    static async kill(interaction) {
        const queryres = await Tickets.destroy({ where: {}, truncate: true });
        if (!queryres) return interaction.reply({ content: 'Something went wrong with truncate the ticket table!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
        return interaction.reply({ content: 'Support ticket table was truncate successfully!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    }

};