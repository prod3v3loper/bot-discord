# 🤖 DISCORD BOT

The bot for your community. 

> With our own bot we protect the privacy of our community. 

# FEATURES

- [x] ✅ Verification with **WAX wallet** - [Wax Cloud Wallet](https://www.mycloudwallet.com/) or with a **BSC Wallet** - [Binance Smart Chain](https://academy.binance.com/en/articles/the-best-crypto-wallets-for-binance-smart-chain-bsc)

- [x] 💼 Wallet System
- [x] 🪙 Coin System
- [x] 📧 Ticket System 
- [x] 📰 News System
- [x] 🥉 Leaderboard System
- [x] 🕹️ Gaming System
- [x] 🥇 Ranking System
- [x] 🪪 Profile System
- [x] 🎁 Giveaway System
- [x] 📊 Level System
- [x] ❗️ Warn System
- [x] ❔ Help System

## TODOS

- [ ] ✅ Verification with email

- [ ] 🎗️ Award System
- [ ] 🌏 Web3 connection

# DEPLOYED COMMANDS

Slash Comands

`/help`

`/server`

`/user`

`/social`

ou can adjust in `.env` file box color, thumbnail author infos:

```bash
SOCIALTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
SOCIALCOLOR='#0099ff'
SOCIALAUTHORNAME='📱 SOCIAL LINKS'
SOCIALAUTHORURL='https://www.prod3v3loper.com/'
SOCIALAUTHORTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'

HELPTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
HELPCOLOR='#0099ff'
HELPAUTHORNAME='🛟 HELP INFO'
HELPAUTHORURL='https://www.prod3v3loper.com/'
HELPAUTHORTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'

USERTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
USERCOLOR='#0099ff'
USERAUTHORNAME='🙍‍♂️ USER DATA'
USERAUTHORURL='https://www.prod3v3loper.com/'
USERAUTHORTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'

SERVERTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
SERVERCOLOR='#0099ff'
SERVERAUTHORNAME='🖥️ SERVER DATA'
SERVERAUTHORURL='https://www.prod3v3loper.com/'
SERVERAUTHORTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
```

# REQUIERD FOR PROJECT

Create your bot first: https://discord.com/developers/applications

Developer portal: https://discord.com/developers/docs/intro

## NODEJS

We need for this for the project [NodeJS](https://nodejs.org/en), also installed.

## NODEMON

And then we need npm package `nodemon` for developing, installed too global or local.

Global:

```bash
$ npm install -g nodemon
```

Or local:

```bash
$ npm install nodemon --save
```

Check:

```bash
$ nodemon -v
```

## PM2

Or better use pm2 for production:

Global:

```bash
$ sudo npm install -g pm2
```

Or local:

```bash
$ npm install pm2 --save
```

Check: 

```bash
$ pm2 --version
```

To startup on systemstart:

```bash
$ pm2 startup
```

```bash
$ sudo env PATH=$PATH:/usr/bin pm2 startup systemd -u pi --hp /home/pi
```

```bash
$ pm2 save
```

# INSTALL PROJECT

> How best to start using it.

Create a new project folder or switch to an existing one and clone the repo first.

**Clone project**

```bash
$ git clone https://gitlab.com/prod3v3loper/bot-discord.git
```

After that we need all the packages that are in the `package.json`
To install them in your project folder enter the following.

**Install all packages we need**

```bash
$ npm install
```

# CORE SETTINGS

We have two config files. Which can be filled as desired and used depending on the purpose.

`config.json`

1. Create a bot and get tokens

https://discord.com/developers/applications

```json
{
  "token": "bot-token", /* https://discord.com/developers/applications - Create here or reset for new token */
  "guildId": "server-id", /* Your Discord server id copy from your discord and insert here */
  "clientId": "application-id or client-id" /* https://discord.com/developers/applications - Create here or copy if exists */
}
```

2. Invite bot: Generate the invitation link with the bot's client ID and the required permissions. The generated link should look something like this:

https://discord.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&scope=bot&permissions=8

In this example, permission 8 is used for admin rights. This can be created and viewed on the discord page where the bot is created.

3. Use invite link: Use this invite link to invite the bot to your server. This means opening the link in your browser and selecting the server you want to add the bot to.

4. Check permissions: Make sure the bot has the necessary permissions to function correctly.

# DATABASE

Sequelize is an object-relational-mapper, which means you can write a query using objects and have it run on almost any other database system that Sequelize supports.

- https://discordjs.guide/sequelize/

- https://sequelize.org/docs/v6/getting-started/

- https://sequelize.org/docs/v6/other-topics/migrations/

`.env`

```bash
NODE_ENV=test

# SQLite3 with local database
DBHOST=db-host
DBPORT=db-port
DBNAME=db-name
DBUSER=db-user
DBPASS=db-pass
DBDIALECT='sqlite'
DBSTORAGE='database.sqlite'

# MySQL can connect with your website and other
DB_HOST='localhost'
DB_PORT='3306'
DB_DRIVER='mysql'
DB_NAME='dbname'
DB_TABLE='discord'
DB_USER='root'
DB_PASS='test'
DB_PREFIX='in_'
```

## SQLite3 MIGRATIONS

```bash
$ npx sequelize --help
```

```bash
$ npx sequelize init
```
Init creates the configs, but is it already created here

```bash
project/
├── config
│   └── config.json
├── migrations
├── models
│   └── index.js
└── seeders
```

Create migrations:

```bash
$ npx sequelize migration:create --name <MIGRATION-NAME>
```

Example:

```bash
$ npx sequelize migration:generate --name create-users
```

Synchronize:

```bash
$ npx sequelize db:migrate
```

Remove:

```bash
$ npx sequelize db:migrate:undo
```

# DEVELOP USAGE

First deploy the commands on first usage:

**Deploy commands**

```bash
$ npm run deploy
```

> You can use the deploy to update commands.

To run and develop the bot use this commands with node and nodemon.

> The slqite3 database was created on the first start for the

**Run dev envoirement with**

```bash
$ npm run dev
```

**Run production envoirement with test**

```bash
$ npm run prod
```

**Run dev with displaying trace-warnings**

```bash
$ npm run test
```

**Stop all node processes**

```bash
$ npm run pkill
```

**Node process kill if needed for NodeJs CI**

```bash
$ npm run kill
```

With pm2 for run non-stop in production:

```bash
$ npm run start
```

```bash
$ npm run stop
```

```bash
$ npm run restart
```

```bash
$ npm run delete
```

```bash
$ npm run logs
```

# BOT SETTINGS

All fields must be filled in for everything to work.

Full `.env` file:

```bash
# NODE

NODE_ENV=test

# SQLite3 with local database

DBHOST=db-host
DBPORT=db-port
DBNAME=db-name
DBUSER=db-user
DBPASS=db-pass
DBDIALECT='sqlite'
DBSTORAGE='database.sqlite'

# MySQL can connect with your website and other

DB_HOST='localhost'
DB_PORT='3306'
DB_DRIVER='mysql'
DB_NAME='dbname'
DB_TABLE='discord'
DB_USER='root'
DB_PASS='test'
DB_PREFIX='in_'

# APIs to check wallets

BINANCE_API_KEY=''

# BOT 

BOTSTATUS='over you 🤫'
BOTTYPE=3
BOTURL=''

NAME=''
LINK=''
WEBSITE=''

INSTAGRAMLINK=''
TWITTERLINK=''
WAXLINK=''
OPENSEALINK=''
DISCORDINVITELINK=''

# COMMANDS

SOCIALTHUMB=''
SOCIALCOLOR='#0099ff'
SOCIALAUTHORNAME='📱 SOCIAL LINKS'
SOCIALAUTHORURL=''
SOCIALAUTHORTHUMB=''

HELPTHUMB=''
HELPCOLOR='#0099ff'
HELPAUTHORNAME='🛟 HELP INFO'
HELPAUTHORURL=''
HELPAUTHORTHUMB=''

USERTHUMB=''
USERCOLOR='#0099ff'
USERAUTHORNAME='🙍‍♂️ USER DATA'
USERAUTHORURL=''
USERAUTHORTHUMB=''

SERVERTHUMB=''
SERVERCOLOR='#0099ff'
SERVERAUTHORNAME='🖥️ SERVER DATA'
SERVERAUTHORURL=''
SERVERAUTHORTHUMB=''

# NEWS

NEWSCOLOR='0x0099FF'
NEWSURL=''
NEWSTHUMB=''
NEWSIMG=''
NEWSTITLE='NEWS'
MAINTITLE='We have some new stuff for you!'
MAINTITLE1='NFT'
MAINTITLE2='CRYPTO'
MAINTITLE3='BOT'
NEWSDESC='News description'
MAINDESC=''
MAINDESC1='Our NFTs WAX and OpenSea'
MAINDESC2='Our Coin AKCE in Binance Smart Chain $AKE'
MAINDESC3='To manage all and help you with everything'
NEWSAUTHOR='PROD3V3LOPER'
NEWSAUTHORURL=''
NEWSAUTHORTHUMB=''
NEWSFOOTERTHUMB=''
NEWSFOOTERCOPY='prod3v3loper (c) All rights reserved'

# GENERALs

COLOR='0x0099FF'
URL='https://www.prod3v3loper.com/'
THUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
AUTHOR='PROD3V3LOPER'
AUTHORURL='https://www.prod3v3loper.com/'
AUTHORTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
FOOTERTHUMB='https://www.prod3v3loper.com/img/prod3v3loper.png'
FOOTERCOPY='prod3v3loper (c) All rights reserved'

# GIVEAWAYs

GIVEAWAYCHANNELID=
GIVEAWAYURL=
GIVEAWAYTHUMB=

# IDs

GUILDID=
ADMINID=
MODERATORID=

# CHANNELs

RULESCHANNEL=
ACCESSCHANNEL=
UPDATECHANNEL=
ANNOUNCEMENTCHANNEL=
BOTCOMMANDCHANNEL=
WELCOMECHANNELID=
WELCOMEMESSAGE='Welcome to our server.'
VERIFYCHANNELID=
VERIFYCHANNELID2=
MAINCHATCHANNELID=
TICKETCHANNELID=
MUSICCHANELID=
GAMECHANNELID=
NFTCHANNELID=
NFTCHOOSECHANNELID=
EXTRAROLECHANNELID=
EXTRAROLE2CHANNELID=

# ROLEs

VERIFEDROLE=
UNVERIFIEDROLE=

CUSTOMROLESNAME=
CUSTOMROLEIID=
CUSTOMROLESNAME1=
CUSTOMROLEIID1=
CUSTOMROLESNAME2=
CUSTOMROLEIID2=
CUSTOMROLESNAME3=
CUSTOMROLEIID3=

EXTRAROLEID=
EXTRAROLENAME=
EXTRAROLE2ID=
EXTRAROLE2NAME=

# GAME COIN

COIN='**p3**'
COINIMAGE=''
```

# BOT USAGE

This commands are available in discord after the bot is connected successfully.

## ADMIN

Setup a ticket system

```bash
!ticket setup #channel
```

Setup a giveaway dashboard with giveaway in custom channel 

```bash
!giveaway setup #channel PRICE, TIME, WINNER
```

Warn user actions

```bash
!warn setup @user #channel warning-message
```

```bash
!warn remove @user
```

Leaderboard actions

```bash
!board setup #channel
```

```bash
!board update #channel
```

Edit for news this fields and post your news ;)

```bash
NEWSCOLOR='0x0099FF'
NEWSURL=''
NEWSTHUMB=''
NEWSIMG=''
NEWSTITLE='NEWS'
MAINTITLE='We have some new stuff for you!'
MAINTITLE1='NFT'
MAINTITLE2='CRYPTO'
MAINTITLE3='BOT'
NEWSDESC='News description'
MAINDESC=''
MAINDESC1='Our NFTs WAX and OpenSea'
MAINDESC2='Our Coin AKCE in Binance Smart Chain $AKE'
MAINDESC3='To manage all and help you with everything'
NEWSAUTHOR='PROD3V3LOPER'
NEWSAUTHORURL=''
NEWSAUTHORTHUMB=''
NEWSFOOTERTHUMB=''
NEWSFOOTERCOPY='prod3v3loper (c) All rights reserved'
```

And now post with in the channel you want from your admin area

```bash
!news setup #channel
```

Wallet actions

```bash
.wallet update @user #wax.wallet
```

```bash
.wallet remove @user
```

List verified users

```bash
.whitelist
```

Coin actions

```bash
!coins remove @user
```

XP actions

```bash
!xp remove @user
```

## MEMEBER

### Wallet

Check wallets

```bash
.wallets
```

Add a wallet 

```bash
.wallet add #wax.wallet
```

Verifys the wax wallet address

```bash
.wallet verify
```

Checks if have nfts in wallet

```bash
.wallet nfts
```

### Coins

Check coins

```bash
!coins
```

Get daily coins

```bash
!daily
```

### XP

```bash
!level
```

### Rank

Check ranks

```bash
!rank
```

Show list of ranks

```bash
!buyrank list
```

Buy a rank

```bash
!buyrank Janissary
```

### Game

Flip game example

```bash
!flip 2 head
```

### Profile

Displays profile

```bash
!profile
```

Displays the card style

```bash
!card
```

# FRAMEWORK

This bot is built on NodeJs with discord.js 14 and Sqlite3

- https://discordjs.guide/#before-you-begin

# ISSUE

Please use the issue tab to request a:

* Bug
* Feature

Choose template and report a bug or feature you want [issues](https://gitlab.com/prod3v3loper/bot-discord/issues).

# CONTRIBUTE

Please read the [contributing](https://gitlab.com/prod3v3loper/bot-discord/blob/main/) to contribute.

# VULNERABILITY

Please use the Security section for privately reporting a [vulnerability](https://gitlab.com/prod3v3loper/bot-discord/security).

# LICENSE

[MIT](https://gitlab.com/prod3v3loper/bot-discord/blob/main/LICENSE)
