/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { EmbedBuilder } = require('discord.js');

const Xps = require('../../models/xp.js');

const MSG = require('./msg.js');
const WALLET = require('./wallet.js');
const VERIFY = require('./verify.js');

/**
 * XP SYSTEM - CRUD
 */
module.exports = class XP {

  static create(channel, message, user, userXp) {

    if (userXp === null || !userXp) {

      const queryres = Xps.create({
        userId: message.author.id,
        xp: 0,
        regxp: 100,
        level: 1
      });

      const errormsg = new EmbedBuilder().setColor('#FF0000').setTitle('Ohhh!').setDescription('Something went wrong with adding your wallet. Try again later or inform the admin.');

      if (!queryres) return MSG.post(channel, errormsg);
    }
  }

  static async read(userId) {
    return await Xps.findOne({ where: { userId: userId } });
  }

  static update(channel, message, user, userXp) {

    if (!user || !userXp) return;

    const addXP = Math.floor(Math.random() * 8) + 3;
    const xpfile = [];

    xpfile[message.author.id] = {
      xp: userXp.xp,
      level: userXp.level,
      regxp: userXp.regxp
    };

    xpfile[message.author.id].xp += addXP;

    if (xpfile[message.author.id].xp > xpfile[message.author.id].regxp) {

      xpfile[message.author.id].xp -= xpfile[message.author.id].regxp;
      xpfile[message.author.id].regxp *= 1.25;
      xpfile[message.author.id].regxp = Math.floor(xpfile[message.author.id].regxp);
      xpfile[message.author.id].level += 1;

      channel.send(`${message.author}` + ' is now level **' + xpfile[message.author.id].level + '**!');
    }

    Xps.update(
      {
        xp: xpfile[message.author.id].xp,
        regxp: xpfile[message.author.id].regxp,
        level: xpfile[message.author.id].level
      },
      {
        where: {
          userId: message.author.id
        },
      }
    );
  }

  static delete(channel, message) {
    let userMention = message.mentions.users.first();
    const queryres = Xps.destroy({
      where: {
        userId: userMention.id
      }
    });
    if (!queryres) return channel.reply({ content: 'Something went wrong with deleting the xp!', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
    return channel.reply({ content: 'Xp removed successfully! :white_check_mark:', ephemeral: true }).then(msg => { setTimeout(() => msg.delete(), 5000) });
  }

  static kill() {
    Xps.destroy({
      where: {},
      truncate: true,
    });
  }

  static display(channel, message, user, userXp) {

    if (WALLET.check(channel, user, true)) {
      if (VERIFY.check(channel, user, true)) {

        if (!userXp) {
          this.create(channel, message, user, userXp);
        }

        const emb = new EmbedBuilder()
          .setColor('#0000FF')
          .setTitle('Level')
          .setDescription(
            'Write messages :speech_balloon: and do other stuff to get more **XP**\nTo receive giveaway benefits. <#' + process.env.GIVEAWAYCHANNELID + '>\n'
          )
          .addFields(
            {
              name: 'Level',
              value: userXp && userXp.level ? "```" + userXp.level + "```" : "```0```",
              inline: true,
            },
            {
              name: 'RegXP',
              value: userXp && userXp.xp ? "```" + userXp.xp + '/' + userXp.regxp + "```" : "```0```",
              inline: true,
            },
            {
              name: 'XP',
              value: userXp && userXp.regxp ? "```" + (userXp.regxp - userXp.xp) + "```" : "```0```",
              inline: true,
            }
          );
        MSG.post(channel, emb);
      }
    }
  }

  static async getMax() {
    return await Xps.findAll({
      order: [
        ['xp', 'DESC']
      ]
    });
  }

};
