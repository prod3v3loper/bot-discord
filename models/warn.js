/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

const { Sequelize } = require('sequelize');
const db = require('./db.js');

const Warns = db.sequelize.define('warns', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.TEXT,
  },
  gId: { type: Sequelize.TEXT, defaultValue: 'No Ids' },
  msgIds: { type: Sequelize.TEXT, defaultValue: 'No Ids' },
  warns: { type: Sequelize.INTEGER, defaultValue: 0 },
  timeouts: { type: Sequelize.INTEGER, defaultValue: 0 },
  bans: { type: Sequelize.INTEGER, defaultValue: 0 }
});

module.exports = Warns;
