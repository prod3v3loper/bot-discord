/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

module.exports = {
    name: 'guildMemberRemove',
    once: false,
    async execute(bot, member) {

        if (member) {
            // Welcome channel custom message
            const leavechannel = await bot.channels.cache.get(process.env.LEAVECHANNELID);
            if (leavechannel) leavechannel.send(`${member} has left the server.`);

            // const hasRole = await member.roles.cache.has(process.env.UNVERIFIEDROLE);
            // if (hasRole) {
            // Add remove user unverified role
            // const newUserRole = await member.guild.roles.cache.get(process.env.UNVERIFIEDROLE);
            // if (newUserRole) member.roles.remove(newUserRole);
            // }
        }
    }
};