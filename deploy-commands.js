/**
 * Deploy commands
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * 
 * @see         https://discordjs.guide/creating-your-bot/#creating-configuration-files
 */
const fs = require('node:fs');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { clientId, guildId, token } = require('./config.json');
const path = require('path');

const commands = [];
const commandFiles = fs.readdirSync(path.join(__dirname, './core/commands')).filter((file) => file.endsWith('.js'));
for (const file of commandFiles) {
  const command = require(`./core/commands/${file}`);
  commands.push(command.data.toJSON());
}

const rest = new REST({ version: '9' }).setToken(token);

rest
  .put(Routes.applicationGuildCommands(clientId, guildId), { body: commands })
  .then(() => console.log('Successfully registered application commands.'))
  .catch(console.error);
