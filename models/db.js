/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

'use strict';

require('dotenv').config({ override: true });

const Sequelize = require('sequelize');

const env = process.env.NODE_ENV;
const config = require('../config/config.js');
const db = {};

let sequelize;
if (env == 'prod') {
  console.log('SQL dialect: ' + process.env.DBDIALECT + ' is PROD');
  sequelize = new Sequelize(config.database, config.username, config.password, config.prod);
} else if (env == 'dev') {
  console.log('SQL dialect: ' + process.env.DBDIALECT + ' is DEV');
  sequelize = new Sequelize(config.database, config.username, config.password, config.dev);
} else if (env == 'test') {
  console.log('SQL dialect: ' + process.env.DBDIALECT + ' is TEST');
  sequelize = new Sequelize(config.database, config.username, config.password, config.test);
}

try {
  sequelize.authenticate();
  console.log('DB model sequelize connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect model to the database:', error);
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
