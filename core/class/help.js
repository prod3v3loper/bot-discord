/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

const { EmbedBuilder } = require('discord.js');

const MSG = require('./msg.js');

/**
 * HELP SYSTEM
 */
module.exports = class HELP {

    static checkForHelp(channel, message, words) {

        if (message.content.includes('help')) {

            const helper = new EmbedBuilder()
                .setColor('#ff0066')
                .setThumbnail(process.env.HELPTHUMB)
                .setAuthor({
                    name: process.env.HELPAUTHORNAME,
                    iconURL: process.env.HELPAUTHORTHUMB,
                    url: process.env.HELPAUTHORURL
                })
                .setTitle(`🛟 I'am here to help you ${message.author.username}`)
                .setDescription('I think you need help ❔\n\nBefore asking more questions, please make sure to check out these channels here:\n\n<#' + process.env.ACCESSCHANNEL + '>\n<#' + process.env.UPDATECHANNEL + '>\n<#' + process.env.ANNOUNCEMENTCHANNEL + '>\n<#' + process.env.BOTCOMMANDCHANNEL + '> \n\nIf you are looking for help other help try this: `/help`');

            MSG.post(channel, helper);
        }
    }
}; 