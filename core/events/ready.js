/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

require('dotenv').config();

// Models
const Wallets = require('../../models/wallet.js');
const Xps = require('../../models/xp.js');
const Coins = require('../../models/coin.js');
const Ranks = require('../../models/rank.js');
const Tickets = require('../../models/ticket.js');
const Giveaways = require('../../models/giveaways.js');
const Participate = require('../../models/participate.js');
const Warns = require('../../models/warn.js');
const Leaderboard = require('../../models/lb.js');
const Social = require('../../models/social.js');

module.exports = {
  name: 'ready',
  once: false,
  async execute(bot) {

    console.log(`Ready! Logged in as ${bot.user.tag}!`);

    console.log(`I'am on ${bot.guilds.cache.size} guild(s)!`);

    // Sync all tables we have
    Wallets.sync({ force: false });
    Xps.sync({ force: false });
    Coins.sync({ force: false });
    Ranks.sync({ force: false });
    Tickets.sync({ force: false });
    Giveaways.sync({ force: false });
    Participate.sync({ force: false });
    Warns.sync({ force: false });
    Leaderboard.sync({ force: false });
    Social.sync({ force: false });
    console.log('All SQLite3 DB tables just synced!');

    // Set bot activity
    bot.user.setActivity({
      name: process.env.BOTSTATUS,
      type: Number(process.env.BOTTYPE),
      url: process.env.BOTURL
    });
    console.log('Bot activity set!');

    var os = require("os");
    if (os.hostname().includes('fv-az')) {
      process.exit(0);
    }
  }
};