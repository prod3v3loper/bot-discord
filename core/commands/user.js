/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 */

const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder().setName('user').setDescription('Replies with user info!'),
  async execute(interaction) {
    const embed = new EmbedBuilder()
      .setAuthor({
        name: process.env.USERAUTHORNAME,
        iconURL: process.env.USERAUTHORTHUMB,
        url: process.env.USERAUTHORURL
      })
      .setThumbnail(process.env.USERTHUMB)
      .setColor(process.env.USERCOLOR)
      .setTitle(interaction.guild.name)
      .setDescription(`
**Your tag:** ${interaction.user.tag}
**Your id:** ${interaction.user.id}`)
      .setFooter({
        text: process.env.FOOTERCOPY,
        iconURL: process.env.FOOTERTHUMB
      })
      .setTimestamp();
    try {
      await interaction.reply({ ephemeral: true, embeds: [embed] });
    } catch (error) {
      console.log(error);
    }
  },
};
