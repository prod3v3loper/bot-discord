/**
 * DISCORD BOT by PROD3V3LOPER
 * 
 * @author      prod3v3loper
 * @copyright   (c) 2023, prod3v3loper
 * @package     Bot
 * @subpackage  Discord
 * @since       1.0
 * @version     1.0
 * @link        https://www.prod3v3loper.com/
 * @see         https://sequelize.org/docs/v6/core-concepts/model-instances/
 * @usage       const { Sequelize, Op, Model, DataTypes } = require('sequelize');
 */

const { Sequelize } = require('sequelize');
const db = require('./db.js');

const Leaderboard = db.sequelize.define('leaderboard', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    userId: {
        type: Sequelize.TEXT,
    },
    gId: { type: Sequelize.TEXT, defaultValue: 'No gId' },
    chId: { type: Sequelize.TEXT, defaultValue: 'No chId' },
    msgId: { type: Sequelize.TEXT, defaultValue: 'No msgId' }
});

module.exports = Leaderboard;